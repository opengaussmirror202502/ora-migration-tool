package com.gbase8c.dmt.config;

import com.gbase8c.dmt.common.util.JsonMapper;
import com.gbase8c.dmt.model.enums.MigrationObjectType;
import lombok.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Slf4j
public class DmtConfig {

    private String workDir;
    private String taskId;
    private int taskPoolCoreSize;
    private int taskPoolMaxSize;

    private List<MigrationObjectType> order;

    public String taskDir() {
        return StringUtils.joinWith(File.separator, workDir, taskId);
    }

    public static DmtConfig read(String workDir, String taskId) {
        String filepath = StringUtils.joinWith(File.separator, workDir, taskId, "dmtConfig.json");
        File file = new File(filepath);
        DmtConfig dmtConfig = JsonMapper.nonEmptyMapper().fromFile(file, DmtConfig.class);
        dmtConfig.setWorkDir(workDir);
        dmtConfig.setTaskId(taskId);
        return dmtConfig;
    }

}

