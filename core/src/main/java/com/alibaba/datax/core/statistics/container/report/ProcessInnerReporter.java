package com.alibaba.datax.core.statistics.container.report;

import com.alibaba.datax.core.statistics.communication.Communication;
import com.alibaba.datax.core.statistics.container.communicator.AbstractContainerCommunicator;
import org.apache.commons.lang3.Validate;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ProcessInnerReporter extends AbstractReporter {

    protected Map<Integer, Communication> taskCommunicationMap;

    protected Map<Integer, Communication> taskGroupCommunicationMap;

    public ProcessInnerReporter(Map<Integer, Communication> taskCommunicationMap,
                                Map<Integer, Communication> taskGroupCommunicationMap) {
        this.taskCommunicationMap = taskCommunicationMap;
        this.taskGroupCommunicationMap = taskGroupCommunicationMap;
    }

    @Override
    public void reportJobCommunication(Long jobId, Communication communication) {
        // do nothing
    }

    @Override
    public void reportTGCommunication(Integer taskGroupId, Communication communication) {
        Validate.isTrue(taskGroupCommunicationMap.containsKey(
        taskGroupId), String.format("taskGroupCommunicationMap中没有注册taskGroupId[%d]的Communication，" +
        "无法更新该taskGroup的信息", taskGroupId));
        taskGroupCommunicationMap.put(taskGroupId, communication);
    }
}