package com.gbase8c.dmt.model.migration.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 保留字冲突设置
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class KeywordsConfig {

    private List<KeywordsMapper> keywordsMappers;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class KeywordsMapper {
        // 模式
        private String schema;
        // 对象类型
        private ObjectType objectType;
        // 所属表
        private String table;
        // 源值
        private String src;
        // 目标库保留字
        private String keywords;
        // 转换后的值
        private String tar;
    }

    /**
     * 对象类型
     */
    public enum ObjectType {
        table, column, function;
    }

}
