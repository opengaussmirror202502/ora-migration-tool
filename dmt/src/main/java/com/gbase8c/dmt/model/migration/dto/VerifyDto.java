package com.gbase8c.dmt.model.migration.dto;

import com.gbase8c.dmt.model.migration.report.SchemaStats;
import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VerifyDto {

    private List<SchemaStats> schemaStats;
    private DboDto verifyData;
}
