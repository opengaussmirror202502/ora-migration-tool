package com.gbase8c.dmt.model.migration.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 迁移配置
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MigrateConfig {
    // 是否建表/重建表
    private Boolean recreateTable;
    // 是否导入数据
    private Boolean migrateData;

    // 是否迁移PK
    private Boolean migratePk;
    // 是否迁移检查约束
    private Boolean migrateCheckConstraint;
    // 是否迁移唯一约束
    private Boolean migrateUniqueConstraint;
    // 是否迁移外键
    private Boolean migrateFk;
    // 是否迁移索引
    private Boolean migrateIndex;

    private boolean preserveCase;

    private DmSetting dmSetting;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DmSetting {
        private Integer channel;
        private Integer recordLimit;
        private Integer byteSpeed;
        private Integer batchSize;
    }

    public String tar(String name) {
        if (preserveCase) {
            return name;
        } else {
            return name.toLowerCase();
        }
    }

}
