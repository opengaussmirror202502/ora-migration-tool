package com.gbase8c.dmt.db.object;

import com.gbase8c.dmt.model.migration.dto.ConstraintDto;

import java.util.List;

public interface ConstraintObject extends DbObject<ConstraintDto> {

    String PK = "PK";
    String FK = "FK";
    String UNIQUE = "UNIQUE";
    String CHECK = "CHECK";

    List<ConstraintDto> getConstraintDtos(String schema);

    ConstraintDto getConstraintDto(String schema, ConstraintDto constraintDto);

    List<ConstraintDto> getConstraintDtos(String schema, String table);
}
