package com.gbase8c.dmt.model.migration.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class ConstraintDto extends MigrationObject {
    //所属表名
    private String tableName;
    private String tarTableName;
    //约束名称
    private String constraintSrcName;
    //源约束类型
    private String srcType;
    private String consType;
    //数据源
    private String ds;
    //相关列集合
    private List<TableDto.ColumnDto> columnDtos;
    //相关列集合
    private String columns;
    //Oracle的检查约束语句
    private String searchCondition;

    //目标约束类型
    private String tarType;

    //外键约束时使用主表所属模式
    private String referencedTableSchema;
    //外键约束时使用主表名称
    private String referencedTableName;
    //外键约束时使用主表列
    private List<TableDto.ColumnDto> referencedColumnDtos;
    //外键约束时使用主表列
    private String referencedColumns;

    //外键约束时使用约束名
    private String referencedConsName;

    //对象迁移创建约束语句
    private String tarSql;

    //更新是否级联
    private String updateRule;

    //删除是否级联
    private String deleteRule;

}
