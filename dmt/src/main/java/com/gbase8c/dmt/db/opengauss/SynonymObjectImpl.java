package com.gbase8c.dmt.db.opengauss;

import com.gbase8c.dmt.db.object.SynonymObject;
import com.gbase8c.dmt.model.migration.config.SchemaConfig;
import com.gbase8c.dmt.model.migration.dto.DataSourceDto;
import com.gbase8c.dmt.model.migration.dto.SynonymDto;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Slf4j
// todo 没有具体实现
public class SynonymObjectImpl extends MetaImpl implements SynonymObject {

    public SynonymObjectImpl(DataSourceDto dataSourceDto) {
        super(dataSourceDto);
    }

    @Override
    public List<String> getNames(Map<String, Object> params) {
        String schema = (String) params.get("schema");
        List<String>  sequenceNames = Lists.newArrayList();
        StringBuilder sql = new StringBuilder("SELECT synonym_name FROM all_synonyms WHERE owner = ?");
        sequenceNames = query(sql.toString(), new ColumnListHandler<String>(), schema);
        return sequenceNames;
    }

    @Override
    public SynonymDto get(String name, Map<String, Object> params) {
        String schema = (String) params.get("schema");
        String sql = "SELECT synonym_name as synonymName, table_owner as schema, table_name as synObjName FROM all_synonyms WHERE owner = ? and synonym_name = ?";
        SynonymDto dto = query(sql, new BeanHandler<>(SynonymDto.class), schema, name);
        return dto;
    }

    @Override
    public SynonymDto convert(SynonymDto synonymDto, Map<String, Object> params) {
        Boolean convertible = Boolean.TRUE;
        synonymDto.setConvertible(convertible);
        if (synonymDto.getConvertible()){
            synonymDto.setConvertMsg("已完成");
        } else {
            synonymDto.setConvertMsg("失败");
        }

        return synonymDto;
    }

    @Override
    public String sql(SynonymDto synonymDto, Map<String, Object> params) {
        Map<String,String> map = synonymDto.getTask().getSchemaConfig().getSchemaMappers().stream()
                .collect(Collectors.toMap(SchemaConfig.SchemaMapper::getSrcSchemaName, SchemaConfig.SchemaMapper::getTarSchemaName));
        String synonymName = synonymDto.getName();
        String tarSynSchema = synonymDto.getTarSchema();
        String tarTableSchema = map.get(synonymDto.getObjectOwner());
        if (Objects.isNull(tarTableSchema)) {
            tarTableSchema = synonymDto.getObjectOwner();
        }
        String synObjName = synonymDto.getSynObjName();
        boolean preserveCase = synonymDto.getTask().getMigrateConfig().isPreserveCase();

        //create synonym t_class for test_dmt.t_class
        StringBuilder stringBuilder = new StringBuilder();
        //stringBuilder.append("drop synonym ").append(schema).append(".").append(synonymName).append(";");
        stringBuilder.append("create synonym ").append(wrap(tarSynSchema, true))
                .append(".").append(wrap(synonymName, preserveCase)).append(" for ")
                .append("\"").append(tarTableSchema).append("\"").append(".").append(wrap(synObjName, preserveCase));
        log.info(stringBuilder.toString());
        return stringBuilder.toString();
    }

    @Override
    public List<SynonymDto> getSynonymDtos(String schema) {
        List<SynonymDto> synonymDtos = Lists.newArrayList();
        List<String>  sequenceNames = Lists.newArrayList();
        StringBuilder sql = new StringBuilder("select nspname as schema,synname as name from pg_catalog.pg_synonym s join pg_catalog.pg_namespace p on s.synnamespace  = p.oid where nspname = ? ");
        synonymDtos = query(sql.toString(), new BeanListHandler<>(SynonymDto.class), schema);
        return synonymDtos;
    }
}
