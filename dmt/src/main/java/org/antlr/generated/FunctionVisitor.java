// Generated from D:/21_gbase8cv5_dmt1.0.0_service/dmt-service/src/main/resources\Function.g4 by ANTLR 4.9.2
package org.antlr.generated;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link FunctionParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface FunctionVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link FunctionParser#prog}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitProg(FunctionParser.ProgContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionParser#function}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunction(FunctionParser.FunctionContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionParser#functionName}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionName(FunctionParser.FunctionNameContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionParser#parameters}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameters(FunctionParser.ParametersContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionParser#parameter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParameter(FunctionParser.ParameterContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionParser#modes}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitModes(FunctionParser.ModesContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionParser#returnDataType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitReturnDataType(FunctionParser.ReturnDataTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionParser#dataType}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitDataType(FunctionParser.DataTypeContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionParser#invokerRightsClause}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInvokerRightsClause(FunctionParser.InvokerRightsClauseContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionParser#variables}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariables(FunctionParser.VariablesContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionParser#variable}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitVariable(FunctionParser.VariableContext ctx);
	/**
	 * Visit a parse tree produced by {@link FunctionParser#functionBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFunctionBody(FunctionParser.FunctionBodyContext ctx);
	/**
	 * Visit a parse tree produced by the {@code primaryExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPrimaryExpression(FunctionParser.PrimaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayExpression(FunctionParser.ArrayExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code shiftExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitShiftExpression(FunctionParser.ShiftExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code additiveExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAdditiveExpression(FunctionParser.AdditiveExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code relationalExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitRelationalExpression(FunctionParser.RelationalExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code inclusiveOrExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitInclusiveOrExpression(FunctionParser.InclusiveOrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code conditionalExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConditionalExpression(FunctionParser.ConditionalExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code multiplicativeExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitMultiplicativeExpression(FunctionParser.MultiplicativeExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code conditionalAndExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConditionalAndExpression(FunctionParser.ConditionalAndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code postExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPostExpression(FunctionParser.PostExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code customExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCustomExpression(FunctionParser.CustomExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unaryExpressionNotPlusMinus}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryExpressionNotPlusMinus(FunctionParser.UnaryExpressionNotPlusMinusContext ctx);
	/**
	 * Visit a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAndExpression(FunctionParser.AndExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code arrayAccessExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitArrayAccessExpression(FunctionParser.ArrayAccessExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code conditionalOrExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitConditionalOrExpression(FunctionParser.ConditionalOrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code exclusiveOrExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitExclusiveOrExpression(FunctionParser.ExclusiveOrExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code equalityExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitEqualityExpression(FunctionParser.EqualityExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code unaryExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitUnaryExpression(FunctionParser.UnaryExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code parentExpression}
	 * labeled alternative in {@link FunctionParser#primary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitParentExpression(FunctionParser.ParentExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code literalExpression}
	 * labeled alternative in {@link FunctionParser#primary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitLiteralExpression(FunctionParser.LiteralExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code IdentifierExpression}
	 * labeled alternative in {@link FunctionParser#primary}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIdentifierExpression(FunctionParser.IdentifierExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code integerExpression}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitIntegerExpression(FunctionParser.IntegerExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code floatExpression}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFloatExpression(FunctionParser.FloatExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code characterExpression}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitCharacterExpression(FunctionParser.CharacterExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code stringExpression}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitStringExpression(FunctionParser.StringExpressionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code booleanExpresion}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBooleanExpresion(FunctionParser.BooleanExpresionContext ctx);
	/**
	 * Visit a parse tree produced by the {@code nullExpression}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNullExpression(FunctionParser.NullExpressionContext ctx);
}