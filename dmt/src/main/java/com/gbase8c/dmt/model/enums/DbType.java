package com.gbase8c.dmt.model.enums;

public enum DbType {
    Oracle("Oracle",
            "oracle.jdbc.OracleDriver",
            "jdbc:oracle:thin:@${connectType != 'sid' ? '//'}${host}:${port}${connectType != 'sid' ? '/' : ':'}${connectValue}",
            "com.gbase8c.dmt.db.metadata.OracleMetadata",
            "OR"),
    MySQL("MySQL",
            "com.mysql.jdbc.Driver",
            "jdbc:mysql://${host}:${port}/${dbName}",
            "com.gbase8c.dmt.db.metadata.MySQLMetadata",
            "MY"),
    PostgreSQL("PostgreSQl",
            "org.postgresql.Driver",
            "jdbc:postgresql://${host}:${port}/${dbName}",
            "com.gbase8c.dmt.db.metadata.PostgreSQLMetadata",
            "PG"),
//    SQLServer("SQL Server",
//            "com.microsoft.sqlserver.jdbc.SQLServerDriver",
//            "jdbc:sqlserver://${host}:${port}",
//            null,
//            DbFamily.SQLSERVER,
//            true,
//            DbDirectModel.NONE),

    GBase8s("GBase8s",
            "org.opengauss.Driver",
            "jdbc:opengauss://${host}:${port}/${dbName}",
            "com.gbase8c.dmt.db.metadata.GBase8cMetadata",
            "GS"),

    GBase8c("GBase8c",
            "org.opengauss.Driver",
            "jdbc:opengauss://${host}:${port}/${dbName}",
            "com.gbase8c.dmt.db.metadata.GBase8cMetadata",
            "GC"),
    OpenGauss("OpenGauss",
            "org.opengauss.Driver",
            "jdbc:opengauss://${host}:${port}/${dbName}",
            "com.gbase8c.dmt.db.metadata.OpenGaussMetadata",
            "OG");

    private String dbName;
    private String driverClass;
    private String jdbcUrl;
    private String metadataClass;
    private String shortName;

    DbType(String dbName,
           String driverClass,
           String jdbcUrl,
           String metadataClass,
           String shortName) {
        this.dbName = dbName;
        this.driverClass = driverClass;
        this.jdbcUrl = jdbcUrl;
        this.metadataClass = metadataClass;
        this.shortName = shortName;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getDriverClass() {
        return driverClass;
    }

    public void setDriverClass(String driverClass) {
        this.driverClass = driverClass;
    }

    public String getJdbcUrl() {
        return jdbcUrl;
    }

    public void setJdbcUrl(String jdbcUrl) {
        this.jdbcUrl = jdbcUrl;
    }

    public String getMetadataClass() {
        return metadataClass;
    }

    public void setMetadataClass(String metadataClass) {
        this.metadataClass = metadataClass;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public enum DbVersion {
//        Oracle_8i("Oracle 8i", DbType.Oracle, null, null, null),
//        Oracle_9i("Oracle 9i", DbType.Oracle, null, null, null),
        Oracle_10g("Oracle 10g", DbType.Oracle, null, null, null),
        Oracle_11g("Oracle 11g", DbType.Oracle, null, null, null),
        Oracle_12c("Oracle 12c", DbType.Oracle, null, null, null),
        Oracle_19c("Oracle 19c", DbType.Oracle, null, null, null),

        PostgreSQL_8("PostgreSQL 8.0", DbType.PostgreSQL, null, null, null),
        PostgreSQL_12("PostgreSQL 12.0", DbType.PostgreSQL, null, null, null),
        PostgreSQL_13("PostgreSQL 13.0", DbType.PostgreSQL, null, null, null),

        MySQL_5("MySQL 5.7", DbType.MySQL,null,null,null),
        MySQL_8("MySQL 8.0", DbType.MySQL,"com.mysql.cj.jdbc.Driver","jdbc:mysql://${host}:${port}/${dbName}?serverTimezone=GMT",null),

        GBase8cV5_S2("GBase8cV5_S2.0.0B20",DbType.GBase8c,null,null,null),
        GBase8cV5_S3("GBase8cV5_S3.0.0B07",DbType.GBase8c,null,null,null),


        GBase8s_Lite("GBase8cV5_S3.0.0B07",DbType.GBase8s,null,null,null),
//        OPENGAUSS("Opengauss 3.0.0",DbType.OpenGauss,null,null,null),

        OpenGauss300("OpenGauss300",DbType.OpenGauss,null,null,null);

        private String dbVersionName;
        private DbType dbType;
        private String driverClass;
        private String jdbcUrl;
        private String metadataClass;

        DbVersion(String dbVersionName,
                  DbType dbType,
                  String driverClass,
                  String jdbcUrl,
                  String metadataClass) {
            this.dbVersionName = dbVersionName;
            this.dbType = dbType;
            this.driverClass = driverClass;
            this.jdbcUrl = jdbcUrl;
            this.metadataClass = metadataClass;
        }

        public String getDbVersionName() {
            return dbVersionName;
        }

        public void setDbVersionName(String dbVersionName) {
            this.dbVersionName = dbVersionName;
        }

        public DbType getDbType() {
            return dbType;
        }

        public void setDbType(DbType dbType) {
            this.dbType = dbType;
        }

        public String getDriverClass() {
            return driverClass;
        }

        public void setDriverClass(String driverClass) {
            this.driverClass = driverClass;
        }

        public String getJdbcUrl() {
            return jdbcUrl;
        }

        public void setJdbcUrl(String jdbcUrl) {
            this.jdbcUrl = jdbcUrl;
        }

        public String getMetadataClass() {
            return metadataClass;
        }

        public void setMetadataClass(String metadataClass) {
            this.metadataClass = metadataClass;
        }
    }

}