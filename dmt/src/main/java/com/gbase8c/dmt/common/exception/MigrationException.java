package com.gbase8c.dmt.common.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

public class MigrationException extends RuntimeException{

    public MigrationException() {
        super();
    }

    public MigrationException(String message) {
        super(message);
    }

    public MigrationException(String message, Throwable cause) {
        super(message, cause);
    }

    public MigrationException(Throwable cause) {
        super(cause);
    }

    public static MigrationException asMigrationException(String message) {
        return new MigrationException(message);
    }

    public static MigrationException asMigrationException(Throwable cause) {
        if (cause instanceof MigrationException) {
            return (MigrationException) cause;
        }
        return new MigrationException(getMessage(cause), cause);
    }

    public static MigrationException asMigrationException(String message, Throwable cause) {
        if (cause instanceof MigrationException) {
            return (MigrationException) cause;
        }
        return new MigrationException(message, cause);
    }

    private static String getMessage(Object obj) {
        if (obj == null) {
            return "";
        }

        if (obj instanceof Throwable) {
            StringWriter str = new StringWriter();
            PrintWriter pw = new PrintWriter(str);
            ((Throwable) obj).printStackTrace(pw);
            return str.toString();
            // return ((Throwable) obj).getMessage();
        } else {
            return obj.toString();
        }
    }

}
