package com.gbase8c.dmt.migration.job;

import com.gbase8c.dmt.migration.MoLogger;
import com.gbase8c.dmt.model.enums.MigrationObjectType;
import com.gbase8c.dmt.model.migration.dto.MigrationObject;
import com.gbase8c.dmt.model.migration.record.JobThreadRecord;

import javax.sql.DataSource;

public class IndexJobThread extends AbstractJobThread{

    public IndexJobThread(MigrationObjectType type, MigrationObject dbObject, DataSource tarDataSource, MoLogger moLogger) {
        super(type, dbObject, tarDataSource, moLogger);
    }

    @Override
    public JobThreadRecord call() throws Exception {
        return null;
    }
}
