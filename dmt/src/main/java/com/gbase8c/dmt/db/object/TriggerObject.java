package com.gbase8c.dmt.db.object;

import com.gbase8c.dmt.model.migration.dto.TriggerDto;

import java.util.List;

public interface TriggerObject extends DbObject<TriggerDto> {

    TriggerDto getTriggerDto(String schema, TriggerDto triggerDto);
    List<String> triggerSql(TriggerDto triggerDto);
}
