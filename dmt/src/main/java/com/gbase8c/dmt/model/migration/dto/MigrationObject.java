package com.gbase8c.dmt.model.migration.dto;

import com.gbase8c.dmt.model.enums.MigrationObjectType;
import com.gbase8c.dmt.model.migration.config.Task;
import com.gbase8c.dmt.model.enums.Status;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.StringUtils;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class MigrationObject {
    protected Task task;
    protected String taskId;
    protected MigrationObjectType migrationObjectType;

    protected String schema;
    protected String tarSchema;
    protected String name;
    protected String tarName;

    protected List<String> contents;

    protected Status status = Status.UnStart;

    protected TableMapper tableMapper;

    //是否初始化
    protected Boolean initializable;
    protected Boolean convertible;
    //迁移失败备注
    protected String initMsg;
    //迁移评估报告视图转化结果
    protected String convertMsg;
    //迁移评估报告视图备注信息
    protected String noteMsg;
    protected String msg;

    protected String getTableName() {
        return null;
    }

    public String key() {
        StringBuilder sb = new StringBuilder();
        sb.append(migrationObjectType).append(":");
        if (StringUtils.isNotBlank(schema)) {
            sb.append(schema).append(".");
        }
        if (StringUtils.isNotBlank(getTableName())) {
            sb.append(getTableName()).append(".");
        }
        sb.append(name);
        return sb.toString();
    }
}
