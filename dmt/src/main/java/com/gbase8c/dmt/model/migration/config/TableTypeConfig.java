package com.gbase8c.dmt.model.migration.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 表类型设置
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TableTypeConfig {

    private List<TableTypeMapper> tableTypeMappers;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TableTypeMapper {
        // 模式
        private String schema;
        // 表名
        private String table;
        // 表类型
        private TableType tableType;
        // 字段
        private String column;

        //数据量大小
        private Double dataSize;
    }

    /**
     * 表类型
     */
    public enum TableType {
        distribution,
        replication;
    }
}
