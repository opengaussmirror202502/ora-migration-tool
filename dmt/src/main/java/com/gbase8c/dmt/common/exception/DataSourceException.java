package com.gbase8c.dmt.common.exception;

import java.io.PrintWriter;
import java.io.StringWriter;

public class DataSourceException extends RuntimeException {

    public DataSourceException() {
        super();
    }

    public DataSourceException(String message) {
        super(message);
    }

    public DataSourceException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataSourceException(Throwable cause) {
        super(cause);
    }

    public static DataSourceException asDataSourceException(String message) {
        return new DataSourceException(message);
    }

    public static DataSourceException asDataSourceException(Throwable cause) {
        if (cause instanceof DataSourceException) {
            return (DataSourceException) cause;
        }
        return new DataSourceException(getMessage(cause), cause);
    }

    public static DataSourceException asDataSourceException(String message, Throwable cause) {
        if (cause instanceof DataSourceException) {
            return (DataSourceException) cause;
        }
        return new DataSourceException(message, cause);
    }

    private static String getMessage(Object obj) {
        if (obj == null) {
            return "";
        }

        if (obj instanceof Throwable) {
            StringWriter str = new StringWriter();
            PrintWriter pw = new PrintWriter(str);
            ((Throwable) obj).printStackTrace(pw);
            return str.toString();
            // return ((Throwable) obj).getMessage();
        } else {
            return obj.toString();
        }
    }

}
