package com.gbase8c.dmt.model.migration.config;

import com.google.common.base.Objects;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * 数据类型设置
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataTypeConfig {

    private List<DataTypeMapper> dataTypeMappers;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DataTypeMapper {
        // 源库类型
        private String srcDataType;
        // 长度
        private BigDecimal srcDataLength;
        // 精度
        private Integer srcDataPrecision;
        // 小数点位数
        private Integer srcDataScale;

        // 目标库类型
        private String tarDataType;
        // 长度
        private BigDecimal tarDataLength;
        // 精度
        private Integer tarDataPrecision;
        // 小数点位数
        private Integer tarDataScale;

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DataTypeMapper that = (DataTypeMapper) o;
            return Objects.equal(srcDataType, that.srcDataType)
                    && Objects.equal(srcDataLength, that.srcDataLength)
                    && Objects.equal(srcDataPrecision, that.srcDataPrecision)
                    && Objects.equal(srcDataScale, that.srcDataScale);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(srcDataType, srcDataLength, srcDataPrecision, srcDataScale);
        }
    }

}
