package com.gbase8c.dmt.db.object;

import com.gbase8c.dmt.model.migration.dto.ViewDto;

public interface ViewObject extends DbObject<ViewDto> {
}
