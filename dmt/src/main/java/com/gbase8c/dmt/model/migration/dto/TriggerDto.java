package com.gbase8c.dmt.model.migration.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class TriggerDto extends MigrationObject {

    //是否初始化
    private Boolean initializable;

    /**
     * 触发的表或对象模式名
     */
    private String eventObjectSchema;

    /**
     * 触发的表或对象名
     */
    private String eventObjectTable;

    /**
     * 触发器描述语句:oracle直接拿到,mysql拼接,最终使用:tr_before_insert_employee
     *    BEFORE INSERT
     *    ON t_employee
     *    FOR EACH ROW
     */
    private String description;

    /**
     * 触发时间:before/after
     */
    private String actionTiming;

    /**
     * 触发事件:insert/update/...
     */
    private String triggeringEvent;

    /**
     * 触发器类型mysql使用:row,statment
     */
    private String actionOrientation;

    /**
     * 源触发器主体逻辑 mysql和oracle的begin.end语句
     */
    private String srcTriggerBody;

    /**
     * 目标触发器主体逻辑 mysql和oracle的begin.end语句
     */
    private String tarTriggerBody;

    /**
     * 前端传数据 row
     */
    private String triggerType;

    /**
     * oracle:before each row
     */
    private String oraTriType;

    /**
     * 触发器状态 oracle用:ENABLED
     */
    private String triggerStatus;

    /**
     * openGauss用：EXECUTE PROCEDURE auditlogfunc()
     */
    private String actionStatement;

    /**
     * openGauss用：触发器使用的函数
     */
    private String triggerFuncName;

    private List<String> declareVar;

    //源数量
    private Long srcCount;
    //目标数量
    private Long tarCount;

    private List<String> tarSql;

}
