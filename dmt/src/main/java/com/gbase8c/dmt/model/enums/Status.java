package com.gbase8c.dmt.model.enums;

public enum Status {
    UnStart, InProgress, Finished, Failure;
}
