package com.gbase8c.dmt.model.migration.record;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Mo {

    private String type;
    private String name;
    private String status;
    private String msg;

}
