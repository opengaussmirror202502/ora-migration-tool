package com.gbase8c.dmt.db.object;

import com.gbase8c.dmt.model.migration.dto.TableSpaceDto;

public interface TableSpaceObject extends DbObject<TableSpaceDto> {
}
