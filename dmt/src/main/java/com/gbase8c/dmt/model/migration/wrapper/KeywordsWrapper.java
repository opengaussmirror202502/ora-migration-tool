package com.gbase8c.dmt.model.migration.wrapper;

import com.gbase8c.dmt.model.migration.config.KeywordsConfig;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;

import java.util.List;
import java.util.Map;

public class KeywordsWrapper {

    private Map<String, KeywordsConfig.KeywordsMapper> map = Maps.newHashMap();
    private KeywordsConfig keywordsConfig;

    public KeywordsWrapper(KeywordsConfig keywordsConfig) {
        this.keywordsConfig = keywordsConfig;
        init();
    }

    private void init() {
        if (ObjectUtils.isNotEmpty(keywordsConfig)) {
            List<KeywordsConfig.KeywordsMapper> keywordsMappers = keywordsConfig.getKeywordsMappers();
            if (CollectionUtils.isNotEmpty(keywordsMappers)) {
                for (KeywordsConfig.KeywordsMapper keywordsMapper : keywordsMappers) {
                    KeywordsConfig.ObjectType ot = keywordsMapper.getObjectType();
                    StringBuilder sb = new StringBuilder();
                    sb.append(ot).append(":")
                            .append(keywordsMapper.getSchema()).append(".");
                    switch (ot) {
                        case table:
                        case function:
                            sb.append(keywordsMapper.getSrc());
                            break;
                        case column:
                            sb.append(keywordsMapper.getTable()).append(".");
                            sb.append(keywordsMapper.getSrc());
                            break;
                    }
                    map.put(sb.toString(), keywordsMapper);
                }
            }
        }
    }

    public String getFunctionName(String schema, String functionName, boolean preserveCase) {
        String key = KeywordsConfig.ObjectType.function
                + ":"
                + schema
                + "."
                + functionName;
        if (map.containsKey(key)) {
            return map.get(key).getTar();
        } else {
            return preserveCase(functionName, preserveCase);
        }
    }

    public String getTableName(String schema, String tableName, boolean preserveCase) {
        String key = KeywordsConfig.ObjectType.table
                + ":"
                + schema
                + "."
                + tableName;
        if (map.containsKey(key)) {
            return map.get(key).getTar();
        } else {
            return preserveCase(tableName, preserveCase);
        }
    }

    public String getColumnName(String schema, String tableName, String columnName, boolean preserveCase) {
        String key = KeywordsConfig.ObjectType.column
                + ":"
                + schema
                + "."
                + tableName
                + "."
                + columnName;
        if (map.containsKey(key)) {
            return map.get(key).getTar();
        } else {
            return preserveCase(columnName, preserveCase);
        }
    }

    private String preserveCase(String value, boolean preserveCase) {
        return preserveCase ? value : value.toLowerCase();
    }

}
