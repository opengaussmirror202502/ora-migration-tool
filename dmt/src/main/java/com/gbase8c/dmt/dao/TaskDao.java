package com.gbase8c.dmt.dao;

import com.gbase8c.dmt.model.migration.config.Task;

public interface TaskDao extends Dao<Task> {

}
