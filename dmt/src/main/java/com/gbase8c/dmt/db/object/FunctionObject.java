package com.gbase8c.dmt.db.object;

import com.gbase8c.dmt.model.migration.dto.FunctionDto;

public interface FunctionObject extends DbObject<FunctionDto> {
}
