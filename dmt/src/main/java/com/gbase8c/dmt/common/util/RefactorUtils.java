package com.gbase8c.dmt.common.util;

import java.lang.reflect.Field;

public class RefactorUtils {
    public static boolean setFieldValueByName(Object object,String fieldName,Object fieldValue){
//        boolean res = false;
        Class<?> clz = object.getClass();
        while(clz!=null){
            try{
                Field field = clz.getDeclaredField(fieldName);
                field.setAccessible(true);
                field.set(object,fieldValue);
                return true;
            }catch (Exception e){
                e.printStackTrace();
                throw new IllegalStateException(e);
            }
        }
        return false;
    }


    public static <V> V getFieldValueByName(Object object,String fieldName){
        Class<?> clz = object.getClass();
        while(clz != null){
            try{
                Field field = clz.getDeclaredField(fieldName);
                field.setAccessible(true);
                return (V)field.get(object);
            }catch (Exception e){
                e.printStackTrace();
                throw new IllegalStateException(e);
            }
        }
        return null;
    }

}
