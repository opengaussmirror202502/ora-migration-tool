package com.gbase8c.dmt.model.enums;

public enum MigrationTaskStatus {
    UNSTART,
    COLLECTING,
    CONVERTING,

    PRECHECKING,
    PRECHECKED_SUCCESS,
    PRECHECKED_FAILED,

    RUNNING,

    SUCCESS,
    FAILED
}
