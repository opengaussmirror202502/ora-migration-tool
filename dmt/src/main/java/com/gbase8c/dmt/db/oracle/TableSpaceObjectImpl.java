package com.gbase8c.dmt.db.oracle;

import com.gbase8c.dmt.db.object.TableSpaceObject;

import com.gbase8c.dmt.model.migration.dto.DataSourceDto;
import com.gbase8c.dmt.model.migration.dto.TableSpaceDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;
import org.apache.commons.dbutils.handlers.ScalarHandler;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

@Slf4j
public class TableSpaceObjectImpl extends MetaImpl implements TableSpaceObject {

    public TableSpaceObjectImpl(DataSourceDto dataSourceDto) {
        super(dataSourceDto);
    }

    @Override
    public List<String> getNames(Map<String, Object> params) {
        String sql = "select tablespace_name as tableSpaceName from dba_tablespaces where tablespace_name not in ('EXAMPLE','INDX','ODM','TOOLS','USERS','XDB','SYSTEM','UNDOTBS1','TEMP','CWMLITE','DRSYS','SYSAUX') and contents <> 'TEMPORARY'";
        List<String> tableSpaceNames = query(sql, new ColumnListHandler<String>());
        return tableSpaceNames;
    }

    @Override
    public TableSpaceDto get(String name, Map<String, Object> params) {
        String sql = "select tablespace_name AS name,file_name AS srcPath from Dba_Data_Files where tablespace_name = ? ";
        TableSpaceDto tableSpaceDto = query(sql, new BeanHandler<>(TableSpaceDto.class), name);
        String maxSizeSql = "select sum(MAXBYTES)/1024 from Dba_Data_Files where tablespace_name = ? ";
        BigDecimal sizeSumKB = query(maxSizeSql, new ScalarHandler<>(), name);
        // todo 如果sizeSumKB为null, 应给出默认值
        tableSpaceDto.setSpcMaxSize(sizeSumKB + "K");
        return tableSpaceDto;
    }

    @Override
    public TableSpaceDto convert(TableSpaceDto tableSpaceDto, Map<String, Object> params) {
       return null;
    }

    @Override
    public String sql(TableSpaceDto tableSpaceDto, Map<String, Object> params) {
        return null;
    }
}
