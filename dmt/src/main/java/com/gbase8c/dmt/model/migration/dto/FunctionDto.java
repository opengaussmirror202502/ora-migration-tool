package com.gbase8c.dmt.model.migration.dto;

import com.gbase8c.dmt.model.migration.config.Type;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class FunctionDto extends Type {
    private String ds;
    private String srcFuncText;
    private String tarFuncText;

    //opengauss函数使用
    private String specificName;

    private List<Parameter> paramDtos;
    private List<Variable> varDtos;

    private String srcFunctionBody;
    private String tarFunctionBody;

//    private String returnExpression;
    //函数sql
    private String tarSql;

    @Getter
    @Setter
    @SuperBuilder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Parameter extends Type {
        private String paramName;
        private List<Mode> modes;

        private Object defaultValue;
    }

    @Getter
    @Setter
    @SuperBuilder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Variable extends Type {
        private String varName;
        private Object defaultValue;

    }

    public enum Mode {
        IN, OUT;
    }
}
