package com.gbase8c.dmt.model.migration.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class PartitionInfoDto {

    // 分区名
    private String partitionName;

    // 分区区间值
    private Object highValue;

    // 分区排名
    private Long partitonPosition;

//    @NotSaved
    private Object value;
    //
    private String dataType;

    private List<PartColumnDto> partColumnDtoList;
    private List<SubPartitionDto> subPartitionDtoList;

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class PartColumnDto {
        // 分区列名
        private String partColumnName;

        // 分区列数据类型
        private String dataType;

        // 列区间值
        private Object highValue;

        //    @NotSaved
        private Object value;
    }

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SubPartitionDto {
        // 分区名
        private String subPartitionName;

        // 分区区间值
        private Object subPartHighValue;

        private String subDataType;

        //    @NotSaved
        private Object subValue;

        private String subPartitionBy;
    }

}