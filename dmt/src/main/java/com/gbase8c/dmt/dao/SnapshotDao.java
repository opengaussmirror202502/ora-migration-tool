package com.gbase8c.dmt.dao;

import com.gbase8c.dmt.model.migration.config.Snapshot;

import java.util.List;

public interface SnapshotDao extends Dao<Snapshot> {

    List<Snapshot> list(String dsId);

}
