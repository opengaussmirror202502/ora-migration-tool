package com.gbase8c.dmt.model.migration.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 表设置
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TableConfig {
    // 模式
    private String srcSchema;

    // 该模式下所有的表(后台用)
    private List<String> tableNames;
    // 迁移表范围
    private ScopeType scopeType;
    // 对应的值
    private List<String> values;

    /**
     * 迁移表范围
     */
    public enum ScopeType {
        full, include, exclude;
    }
}
