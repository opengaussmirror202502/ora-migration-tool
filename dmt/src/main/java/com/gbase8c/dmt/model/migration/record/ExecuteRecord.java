package com.gbase8c.dmt.model.migration.record;

import com.gbase8c.dmt.model.enums.MigrationObjectType;
import com.gbase8c.dmt.model.migration.dto.MigrationObject;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
// 任务执行记录
public class ExecuteRecord {

    // 任务类型
    private MigrationObjectType type;

    // 模式名
    private String schemaName;

    // 错误对象列表，记录执行失败任务的对象实体
    private List<MigrationObject> errorObjs;

    // 成功对象列表，记录执行失败任务的对象实体
    private List<MigrationObject> successObjs;

    // 错误日志列表，记录执行失败任务的日志
    private List<String> errorLogs;

    // 执行总数
    private int total;

    // 执行成功数
    private int succeed;

    // 执行失败数
    private int failed;

    // 忽略执行数
    private int ignored;


}
