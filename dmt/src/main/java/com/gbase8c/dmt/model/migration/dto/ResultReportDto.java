package com.gbase8c.dmt.model.migration.dto;

import lombok.*;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ResultReportDto {
    private ContentDto contentDto;
    private VerifyDto verifyDto;
}
