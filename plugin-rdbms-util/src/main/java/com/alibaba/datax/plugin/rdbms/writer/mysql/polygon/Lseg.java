package com.alibaba.datax.plugin.rdbms.writer.mysql.polygon;

public class Lseg implements IPolygon{
    private Point startPoint;
    private Point endPoint;

    public Point getStartPoint() {
        return startPoint;
    }

    public void setStartPoint(Point startPoint) {
        this.startPoint = startPoint;
    }

    public Point getEndPoint() {
        return endPoint;
    }

    public void setEndPoint(Point endPoint) {
        this.endPoint = endPoint;
    }

    @Override
    public String toString() {
        return String.format("[%s,%s]", startPoint,endPoint);
    }

    @Override
    public String toGBase8cStringValue() {
        return String.format("[%s,%s]", startPoint,endPoint);
    }
}
