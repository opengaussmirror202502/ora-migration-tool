package com.gbase8c.dmt.model.migration.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class SynonymDto extends MigrationObject {

    // 同义词涉及到的模式
    private String objectOwner;

    // 同义词涉及到的对象
    private String synObjName;

    private String tarSql;

}
