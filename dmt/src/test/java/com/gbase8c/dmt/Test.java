package com.gbase8c.dmt;

import com.alibaba.datax.common.util.Configuration;
import com.alibaba.datax.core.Engine;
import com.alibaba.datax.core.util.ConfigParser;

public class Test {

    public static void main(String[] args) {;
        System.setProperty("datax.home", "D:/ora-migration-tool/target/ora-migration-tool/ora-migration-tool");
        System.setProperty("datax.persistence", "true");

        Configuration configuration = ConfigParser.parse("D:/workdir/test.json");
        configuration.set("core.container.model", "taskGroup");

//        Configuration configuration = ConfigParser.parse("D:/workdir/ori.json");

        Engine engine = new Engine();
        engine.start(configuration);

    }

}
