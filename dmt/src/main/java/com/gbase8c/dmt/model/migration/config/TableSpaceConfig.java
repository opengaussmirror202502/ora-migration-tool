package com.gbase8c.dmt.model.migration.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 表空间配置
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TableSpaceConfig {

    private List<TableSpaceMapper> tableSpaceMappers;

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TableSpaceMapper {
        // 表空间
        private String tableSpaceName;
        // 源路径
        private String srcPath;
        // 目标路径
        private String tarPath;
        //表空间所有者（owner）
        private String useName;

        //
        private String relative;

        //
        private String spcMaxSize;

        //
        private String spcOptions;
    }
}
