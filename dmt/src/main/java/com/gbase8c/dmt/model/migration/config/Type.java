package com.gbase8c.dmt.model.migration.config;

import com.gbase8c.dmt.model.migration.dto.MigrationObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class Type extends MigrationObject {
    protected String srcType;
    protected String dataType;
    protected Integer sqlType;
    protected Integer dataLength;
    protected Integer dataPrecision;
    protected Integer dataScale;

    protected String tarType;
    protected String tarDataType;
    protected Integer tarDataLength;
    protected Integer tarDataPrecision;
    protected Integer tarDataScale;

}
