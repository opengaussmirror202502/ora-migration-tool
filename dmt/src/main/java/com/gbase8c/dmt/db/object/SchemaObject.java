package com.gbase8c.dmt.db.object;

import com.gbase8c.dmt.model.migration.dto.SchemaDto;

import java.util.List;
import java.util.Map;

public interface SchemaObject extends DbObject<SchemaDto> {

    List<String> getNames(Map<String, Object> params, boolean includeSystemSchemas);

}
