package com.gbase8c.dmt.dao;

import com.gbase8c.dmt.model.migration.dto.DataSourceDto;

public interface DataSourceDao extends Dao<DataSourceDto> {

}
