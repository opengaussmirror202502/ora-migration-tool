package org.antlr.generated;

import org.antlr.v4.runtime.*;

import java.util.Collections;
import java.util.List;

public class FunctionErrorListener extends BaseErrorListener {
    @Override
    public void syntaxError(Recognizer<?, ?> recognizer,
                            Object offendingSymbol,
                            int line,
                            int charPositionInLine,
                            String msg,
                            RecognitionException e) {
        List<String> stack = ((Parser) recognizer).getRuleInvocationStack();
        Collections.reverse(stack);
        String ruleStack = "rule stack: " + stack;
        String errorInfo = "line " + line + ":" + charPositionInLine + " at " +
                offendingSymbol + ": " + msg;
        String message = ruleStack + ", " + errorInfo;
        throw new RuntimeException(message, e);
    }
}
