package com.gbase8c.dmt.db.meta;

public class ExtTypes {
    public static final int JSON = 5000;

    public static final int RAW = 5100;
    public static final int UUID = 5101;
    public static final int JSONB = 5102;
    public static final int HLL = 5103;

    public static final int CIDR = 5200;
    public static final int INET = 5201;
    public static final int MACADDR = 5202;

    public static final int ABSTIME = 5300;
    public static final int RELTIME = 5301;
    public static final int SMALLDATETIME = 5302;
    public static final int INTERVAL = 5303;

    public static final int INT4RANGE = 5401;
    public static final int INT8RANGE = 5402;
    public static final int NUMRANGE = 5403;
    public static final int TSRANGE = 5404;
    public static final int TSTZRANGE = 5405;
    public static final int DATERANGE = 5405;

    public static final int TSQUERY = 5500;
    public static final int TSVECTOR = 5501;

    public static final int HASH16 = 5601;
    public static final int HASH32 = 5602;
    public static final int XML = 5603;

    public static final int POINT = 5700;
    public static final int BOX = 5701;
    public static final int CIRCLE = 5702;
    public static final int PATH = 5703;
    public static final int POLYGON = 5704;
    public static final int LSEG = 5705;

    public static final int MONEY = 5800;
    public static final int XMLTYPE = 5801;

    public static final int ENUM = 5900;
    public static final int SET = 5901;
}
