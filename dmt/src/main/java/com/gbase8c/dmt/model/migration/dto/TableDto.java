package com.gbase8c.dmt.model.migration.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class TableDto extends MigrationObject {
    private String ds;
    private String tableSpaceName;
    private List<ColumnDto> columnDtos;
    private List<IndexDto> indexDtos;
    private List<TriggerDto> triggerDtos;

    private String type;
    private String objType;
    private String tableType;
    private String distributeColumn;

    private String tarDsType;

    //对象迁移创建表的sql语句
    private List<String> tarSqls;

    // partitioned true/false
    // when partitioned==true:
    //     partitionBy RANGE/LIST
    //     partitionColumn columnName

    // distributeType DISTRIBUTION/REPLICATION
    // when distributeType==DISTRIBUTION
    //     distributeBy HASH/MODULO/ROUNDROBIN
    //     distributeColumn columnName
    private Map<String, Object> properties;

    //表大小MB
    private Double dataSize;

    //表注释值comment
    private String tableComment;

    //存储opengauss表的options，例：orientation=column,compression=high
    private String tableOptions;

    //存储opengauss表的类型，非日志表unlogged等
    private String relPersistence;

    private ColumnDto autoColumn;

    private List<ColumnDto> enumOrSetColumns;

    //如果查出是分区表,继续查分区表各分区信息
//    private List<Map<String, Object>> partitionInfos;
    private List<PartitionInfoDto> partitionInfos;

    public TableMapper toMapper() {
        List<TableMapper.ColumnMapper> columnMappers = columnDtos.stream()
                .map(dto -> dto.toMapper())
                .collect(Collectors.toList());
        return TableMapper.builder()
                .srcSchemaName(schema)
                .tarSchemaName(tarSchema)
                .srcTableName(name)
                .tarTableName(tarName)
                .columnMappers(columnMappers)
                .build();
    }


    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ColumnDto {
        private String schema;
        private String tableName;
        private String name;
        private String tarName;
        private String dataType;
        private Integer sqlType;

        /**
         * mysql用enum/set类型记录具体内容。例：enum('男','女')
         */
        private String columnType;
        /**
         * mysql用enum/set类型转为8c后，数据类型用此属性代替dataType（为自命名）。
         */
        private String enumOrSetDataType;

        private Long length;

        private BigDecimal dataLength;
        private Integer dataPrecision;
        private Integer dataScale;

        private String nullable;
        /**
         * 列的default默认值
         */
        private String dataDefault;

        /**
         * 列的comment注释值
         */
        private String columnComment;

        /**
         * 列是否自增
         */
        private String extra;
        //自增当前可使用值
        private String autoIncrement;
        private boolean pk;

        private String tarDataType;
        private BigDecimal tarDataLength;
        private Integer tarDataPrecision;
        private Integer tarDataScale;

        public TableMapper.ColumnMapper toMapper() {
            return TableMapper.ColumnMapper.builder()
                    .srcName(name)
                    .tarName(tarName)
                    .build();
        }

    }

}
