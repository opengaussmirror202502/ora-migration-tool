package com.gbase8c.dmt.dao;

import com.gbase8c.dmt.model.migration.record.MigrationRecordDto;

import java.util.List;

public interface RecordDao extends Dao<MigrationRecordDto> {

    List<MigrationRecordDto> list(String taskId);

}
