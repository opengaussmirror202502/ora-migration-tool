package com.gbase8c.dmt.db.oracle;

import com.gbase8c.dmt.db.object.SequenceObject;
import com.gbase8c.dmt.db.object.TableObject;
import com.gbase8c.dmt.model.migration.dto.DataSourceDto;
import com.gbase8c.dmt.model.migration.dto.SequenceDto;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;

import java.util.List;
import java.util.Map;

@Slf4j
public class SequenceObjectImpl extends MetaImpl implements SequenceObject {

    protected TableObject tableObject;

    public SequenceObjectImpl(DataSourceDto dataSourceDto) {
        super(dataSourceDto);
    }

    @Override
    public List<String> getNames(Map<String, Object> params) {
        String schema = (String) params.get("schema");
        List<String>  sequenceDtos = Lists.newArrayList();
        String sql = "select sequence_name as name from dba_sequences where sequence_name not in('LOGMNR_DIDS$'," +
                "'LOGMNR_EVOLVE_SEQ$','LOGMNR_SEQ$','LOGMNR_UIDS$','MVIEW$_ADVSEQ_GENERIC','MVIEW$_ADVSEQ_ID','ROLLING_EVENT_SEQ$') and sequence_owner = ?";
        List<String> sequenceNames = query(sql, new ColumnListHandler<String>(), schema);
        return sequenceNames;

    }

    @Override
    public SequenceDto get(String name, Map<String, Object> params) {
        String schema = (String) params.get("schema");
        String sql = "SELECT sequence_owner as schema, sequence_name as name, min_value as minValue,max_value as maxValue,increment_by as incrementBy, cycle_flag as cycle,cache_size as cacheSize, last_number as lastNumber FROM dba_sequences where sequence_owner = ? and sequence_name = ?";
        SequenceDto dto = query(sql, new BeanHandler<>(SequenceDto.class), schema, name);
        return dto;
    }

    @Override
    public SequenceDto convert(SequenceDto sequenceDto, Map<String, Object> params) {
        String incrementBy = sequenceDto.getIncrementBy();
        String lastNumber = sequenceDto.getLastNumber();
//        Integer temp = Integer.valueOf(incrementBy) + Integer.valueOf(lastNumber);
//        String startWith = String.valueOf(temp);
        sequenceDto.setStartWith(lastNumber);
        Boolean convertible = Boolean.TRUE;
        sequenceDto.setConvertible(Boolean.TRUE);

        sequenceDto.setConvertible(convertible);

        if (sequenceDto.getConvertible()){
            sequenceDto.setConvertMsg("已完成");
        } else {
            sequenceDto.setConvertMsg("失败");
        }

        return sequenceDto;
    }

    @Override
    public String sql(SequenceDto sequenceDto, Map<String, Object> params) {
        String sequenceName = sequenceDto.getName().toLowerCase();
        String schema = sequenceDto.getName().toLowerCase();
        ////创建序列
        //create sequence seq_user_camera_version increment by 1 minvalue 1 no maxvalue start with 1 cache 20 cycle owned by none;
        StringBuilder stringBuilder = new StringBuilder();
//        stringBuilder.append("drop sequence ").append(schema).append(".").append(sequenceName).append(";");
        stringBuilder.append(" create sequence ").append(schema).append(".").append(sequenceName).append(" increment by ")
                     .append(sequenceDto.getIncrementBy()).append(" minvalue ").append(sequenceDto.getMinValue())
        .append(" start with ").append(sequenceDto.getStartWith());

        if (!("0".equals(sequenceDto.getCacheSize())))
        {
            stringBuilder.append(" cache ").append(sequenceDto.getCacheSize());
        }

        if ("Y".equals(sequenceDto.getCycle())){
            stringBuilder.append(" cycle");
        }

        log.info(stringBuilder.toString());
        return stringBuilder.toString();
    }

    @Override
    public List<SequenceDto> getSequenceDtos(String schema) {
        Map<String, Object> params = Maps.newHashMap();
        params.put("schema", schema);

        List<SequenceDto>  sequenceDtos = Lists.newArrayList();
        StringBuilder sql = new StringBuilder("select sequence_owner as schema, sequence_name as name from all_sequences where sequence_name not in('LOGMNR_DIDS$'," +
                "'LOGMNR_EVOLVE_SEQ$','LOGMNR_SEQ$','LOGMNR_UIDS$','MVIEW$_ADVSEQ_GENERIC','MVIEW$_ADVSEQ_ID','ROLLING_EVENT_SEQ$') and sequence_owner = ?");
        sequenceDtos = query(sql.toString(), new BeanListHandler<>(SequenceDto.class),schema);
        return sequenceDtos;
    }

}
