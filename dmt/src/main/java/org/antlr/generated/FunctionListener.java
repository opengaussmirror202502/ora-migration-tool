// Generated from D:/21_gbase8cv5_dmt1.0.0_service/dmt-service/src/main/resources\Function.g4 by ANTLR 4.9.2
package org.antlr.generated;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link FunctionParser}.
 */
public interface FunctionListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link FunctionParser#prog}.
	 * @param ctx the parse tree
	 */
	void enterProg(FunctionParser.ProgContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionParser#prog}.
	 * @param ctx the parse tree
	 */
	void exitProg(FunctionParser.ProgContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionParser#function}.
	 * @param ctx the parse tree
	 */
	void enterFunction(FunctionParser.FunctionContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionParser#function}.
	 * @param ctx the parse tree
	 */
	void exitFunction(FunctionParser.FunctionContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionParser#functionName}.
	 * @param ctx the parse tree
	 */
	void enterFunctionName(FunctionParser.FunctionNameContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionParser#functionName}.
	 * @param ctx the parse tree
	 */
	void exitFunctionName(FunctionParser.FunctionNameContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionParser#parameters}.
	 * @param ctx the parse tree
	 */
	void enterParameters(FunctionParser.ParametersContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionParser#parameters}.
	 * @param ctx the parse tree
	 */
	void exitParameters(FunctionParser.ParametersContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionParser#parameter}.
	 * @param ctx the parse tree
	 */
	void enterParameter(FunctionParser.ParameterContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionParser#parameter}.
	 * @param ctx the parse tree
	 */
	void exitParameter(FunctionParser.ParameterContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionParser#modes}.
	 * @param ctx the parse tree
	 */
	void enterModes(FunctionParser.ModesContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionParser#modes}.
	 * @param ctx the parse tree
	 */
	void exitModes(FunctionParser.ModesContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionParser#returnDataType}.
	 * @param ctx the parse tree
	 */
	void enterReturnDataType(FunctionParser.ReturnDataTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionParser#returnDataType}.
	 * @param ctx the parse tree
	 */
	void exitReturnDataType(FunctionParser.ReturnDataTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionParser#dataType}.
	 * @param ctx the parse tree
	 */
	void enterDataType(FunctionParser.DataTypeContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionParser#dataType}.
	 * @param ctx the parse tree
	 */
	void exitDataType(FunctionParser.DataTypeContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionParser#invokerRightsClause}.
	 * @param ctx the parse tree
	 */
	void enterInvokerRightsClause(FunctionParser.InvokerRightsClauseContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionParser#invokerRightsClause}.
	 * @param ctx the parse tree
	 */
	void exitInvokerRightsClause(FunctionParser.InvokerRightsClauseContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionParser#variables}.
	 * @param ctx the parse tree
	 */
	void enterVariables(FunctionParser.VariablesContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionParser#variables}.
	 * @param ctx the parse tree
	 */
	void exitVariables(FunctionParser.VariablesContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionParser#variable}.
	 * @param ctx the parse tree
	 */
	void enterVariable(FunctionParser.VariableContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionParser#variable}.
	 * @param ctx the parse tree
	 */
	void exitVariable(FunctionParser.VariableContext ctx);
	/**
	 * Enter a parse tree produced by {@link FunctionParser#functionBody}.
	 * @param ctx the parse tree
	 */
	void enterFunctionBody(FunctionParser.FunctionBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link FunctionParser#functionBody}.
	 * @param ctx the parse tree
	 */
	void exitFunctionBody(FunctionParser.FunctionBodyContext ctx);
	/**
	 * Enter a parse tree produced by the {@code primaryExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPrimaryExpression(FunctionParser.PrimaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code primaryExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPrimaryExpression(FunctionParser.PrimaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterArrayExpression(FunctionParser.ArrayExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitArrayExpression(FunctionParser.ArrayExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code shiftExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterShiftExpression(FunctionParser.ShiftExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code shiftExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitShiftExpression(FunctionParser.ShiftExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code additiveExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAdditiveExpression(FunctionParser.AdditiveExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code additiveExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAdditiveExpression(FunctionParser.AdditiveExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code relationalExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterRelationalExpression(FunctionParser.RelationalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code relationalExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitRelationalExpression(FunctionParser.RelationalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code inclusiveOrExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterInclusiveOrExpression(FunctionParser.InclusiveOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code inclusiveOrExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitInclusiveOrExpression(FunctionParser.InclusiveOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code conditionalExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterConditionalExpression(FunctionParser.ConditionalExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code conditionalExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitConditionalExpression(FunctionParser.ConditionalExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code multiplicativeExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterMultiplicativeExpression(FunctionParser.MultiplicativeExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code multiplicativeExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitMultiplicativeExpression(FunctionParser.MultiplicativeExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code conditionalAndExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterConditionalAndExpression(FunctionParser.ConditionalAndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code conditionalAndExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitConditionalAndExpression(FunctionParser.ConditionalAndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code postExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterPostExpression(FunctionParser.PostExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code postExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitPostExpression(FunctionParser.PostExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code customExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterCustomExpression(FunctionParser.CustomExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code customExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitCustomExpression(FunctionParser.CustomExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryExpressionNotPlusMinus}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpressionNotPlusMinus(FunctionParser.UnaryExpressionNotPlusMinusContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryExpressionNotPlusMinus}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpressionNotPlusMinus(FunctionParser.UnaryExpressionNotPlusMinusContext ctx);
	/**
	 * Enter a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterAndExpression(FunctionParser.AndExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code andExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitAndExpression(FunctionParser.AndExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code arrayAccessExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterArrayAccessExpression(FunctionParser.ArrayAccessExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code arrayAccessExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitArrayAccessExpression(FunctionParser.ArrayAccessExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code conditionalOrExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterConditionalOrExpression(FunctionParser.ConditionalOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code conditionalOrExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitConditionalOrExpression(FunctionParser.ConditionalOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code exclusiveOrExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterExclusiveOrExpression(FunctionParser.ExclusiveOrExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code exclusiveOrExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitExclusiveOrExpression(FunctionParser.ExclusiveOrExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code equalityExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterEqualityExpression(FunctionParser.EqualityExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code equalityExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitEqualityExpression(FunctionParser.EqualityExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code unaryExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void enterUnaryExpression(FunctionParser.UnaryExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code unaryExpression}
	 * labeled alternative in {@link FunctionParser#expression}.
	 * @param ctx the parse tree
	 */
	void exitUnaryExpression(FunctionParser.UnaryExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code parentExpression}
	 * labeled alternative in {@link FunctionParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterParentExpression(FunctionParser.ParentExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code parentExpression}
	 * labeled alternative in {@link FunctionParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitParentExpression(FunctionParser.ParentExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code literalExpression}
	 * labeled alternative in {@link FunctionParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterLiteralExpression(FunctionParser.LiteralExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code literalExpression}
	 * labeled alternative in {@link FunctionParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitLiteralExpression(FunctionParser.LiteralExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code IdentifierExpression}
	 * labeled alternative in {@link FunctionParser#primary}.
	 * @param ctx the parse tree
	 */
	void enterIdentifierExpression(FunctionParser.IdentifierExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code IdentifierExpression}
	 * labeled alternative in {@link FunctionParser#primary}.
	 * @param ctx the parse tree
	 */
	void exitIdentifierExpression(FunctionParser.IdentifierExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code integerExpression}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterIntegerExpression(FunctionParser.IntegerExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code integerExpression}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitIntegerExpression(FunctionParser.IntegerExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code floatExpression}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterFloatExpression(FunctionParser.FloatExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code floatExpression}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitFloatExpression(FunctionParser.FloatExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code characterExpression}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterCharacterExpression(FunctionParser.CharacterExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code characterExpression}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitCharacterExpression(FunctionParser.CharacterExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code stringExpression}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterStringExpression(FunctionParser.StringExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code stringExpression}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitStringExpression(FunctionParser.StringExpressionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code booleanExpresion}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterBooleanExpresion(FunctionParser.BooleanExpresionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code booleanExpresion}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitBooleanExpresion(FunctionParser.BooleanExpresionContext ctx);
	/**
	 * Enter a parse tree produced by the {@code nullExpression}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 */
	void enterNullExpression(FunctionParser.NullExpressionContext ctx);
	/**
	 * Exit a parse tree produced by the {@code nullExpression}
	 * labeled alternative in {@link FunctionParser#literal}.
	 * @param ctx the parse tree
	 */
	void exitNullExpression(FunctionParser.NullExpressionContext ctx);
}