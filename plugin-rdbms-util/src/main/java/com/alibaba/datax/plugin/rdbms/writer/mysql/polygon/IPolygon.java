package com.alibaba.datax.plugin.rdbms.writer.mysql.polygon;

public interface IPolygon {

    String toGBase8cStringValue();

}
