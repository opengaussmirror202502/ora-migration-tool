package com.gbase8c.dmt.db.object;

import com.gbase8c.dmt.model.migration.dto.SequenceDto;

import java.util.List;

public interface SequenceObject extends DbObject<SequenceDto> {

    List<SequenceDto> getSequenceDtos(String schema);

}
