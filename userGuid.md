# System Requirements

- Linux
- [JDK(1.8以上，推荐1.8) ](http://www.oracle.com/technetwork/cn/java/javase/downloads/index.html)
- [Apache Maven 3.x](https://maven.apache.org/download.cgi) (Compile)

# Quick Start
#### 注：本插件是在datax的基础上增加了数据库对象迁移，但暂不支持function和trigger的迁移。如现场环境中需迁移function和trigger，请手动迁移。
* 编译部署

  (1)、下载源码：

    ``` shell
    $ git clone ...git
    ```

  (2)、通过maven打包：

    ``` shell
    $ cd  {ora-migration-tool_source_code_home}
    $ mvn -U clean package assembly:assembly -Dmaven.test.skip=true
    ```

  打包成功，日志显示如下：

    ``` 
    [INFO] BUILD SUCCESS
    [INFO] -----------------------------------------------------------------
    [INFO] Total time: 08:12 min
    [INFO] Finished at: 2015-12-13T16:26:48+08:00
    [INFO] Final Memory: 133M/960M
    [INFO] -----------------------------------------------------------------
    ```

  打包成功后的DataX包位于 {DataX_source_code_home}/target/datax/datax/ ，结构如下：

    ``` shell
    $ cd  {ora-migration-tool_source_code_home}
    $ ls ./target/ora-migration-tool/ora-migration-tool/
    bin		conf		job		lib		log		log_perf	plugin		script		tmp
    ```


* 迁移示例：orale->opengauss数据迁移

  * 第一步：准备源数据库和目标数据库，并初始化源数据库。数据库初始化sql脚本为{DataX_source_code_home}/quickstart/src.init.sql

  * 第二步：准备迁移相关文件。将quickstart/wrokdir.zip解压到指定目录
    ``` shell
    cp {ora-migration-tool_source_code_home}/quickstart/workdir.zip /home
    cd /home
    unzip workdir.zip
    ```
    workdir目录说明：
    ```
    .  --${workdir}
    └── 1 --${taskid}
        ├── dbo
        ├── dmtConfig.json --迁移配置文件
        ├── ds             --数据源文件夹
        │   ├── src.json  --数据源文件：${dsid}.json
        │   └── tar.json  --数据源文件：${dsid}.json
        ├── record
        ├── snapshot
        └── task  --迁移任务文件夹
            └── 1.json  --迁移任务：${taskid}.json
    ```
  * 第三步：修改数据源文件相关属性（不要修改文件名和里面的id属性）

  * 第四步：启动迁移

    ``` shell
    windows：
    java -server -Xms1g -Xmx1g -XX:+HeapDumpOnOutOfMemoryError  -XX:HeapDumpPath=${ora-migration-tool.home}/log -Dloglevel=info  -Dfile.encoding=UTF-8  -Dlogback.statusListenerClass=ch.qos.logback.core.status.NopStatusListener  -Djava.security.egd=file:///dev/urandom  -Ddatax.home=${ora-migration-tool.home} -Ddatax.persistence=true -Dlogback.configurationFile="${ora-migration-tool.home}/conf/logback.xml"  -classpath "${ora-migration-tool.home}/lib/*"  com.gbase8c.dmt.Dmt -taskid ${tasked} -workdir ${workdir} [-action rbt] [-tables schema.tn1,schema.tn2...]
    
    其它（mac/linux）：
    java -server -Xms1g -Xmx1g -XX:+HeapDumpOnOutOfMemoryError  -XX:HeapDumpPath=${ora-migration-tool.home}/log -Dloglevel=info  -Dfile.encoding=UTF-8  -Dlogback.statusListenerClass=ch.qos.logback.core.status.NopStatusListener  -Djava.security.egd=file:///dev/urandom  -Ddatax.home=${ora-migration-tool.home} -Ddatax.persistence=true -Dlogback.configurationFile="${ora-migration-tool.home}/conf/logback.xml"  -classpath "${ora-migration-tool.home}/lib/*:." com.gbase8c.dmt.Dmt -taskid ${tasked} -workdir ${workdir} [-action rbt] [-tables schema.tn1,schema.tn2...]
    ```

* 配置文件说明
  * 迁移任务json文件结构说明：
    ```
    {
      "id" : "1", --迁移任务id
      "name" : "test1",  --迁移任务名称
      "src" : "src",  --迁移任务源数据库
      "tar" : "tar",  --迁移任务目标数据库
      "snapshotId" : "1",  --迁移任务快照id
      "schemaConfig" : {  --模型设置
        "schemaMappers" : [ {  --具体模型映射
          "srcSchemaName" : "TEST",  --原模型名
          "tarSchemaName" : "test",  --目标模型名
          "tableMigrated" : true,  --是否迁移表
          "viewMigrated" : true,  --是否迁移视图
          "sequenceMigrated" : true,  --是否迁移序列
          "functionMigrated" : true,  --是否迁移函数
          "synonymMigrated" : true,  --是否迁移同义词
          "triggerMigrated" : true,  --是否迁移触发器
          "tableConfig" : {  --迁移表设置
            "scopeType" : "full",  --full是全部，include是包含，exclude是不包含
            "values": null,  --当scopType为include/exclude时，请给出values的值List<String>
          }
        } ]
      },
      "migrateConfig" : {  --迁移设置
        "recreateTable" : true,  --建表/重建表
        "migrateData" : true,  --是否迁移数据
        "migratePk" : true,  --是否迁移主键
        "migrateCheckConstraint" : true,  --是否迁移check约束
        "migrateUniqueConstraint" : true,  --是否迁移unique约束
        "migrateFk" : true,  --是否迁移外键
        "migrateIndex" : true,  --是否迁移索引
        "preserveCase" : false,  --表名等等（除了schema）是否保持原来的大小写
        "dmSetting" : {  --迁移设置（此设置是datax原生的配置参数）
          "channel" : 2,  --迁移线程（每张表）
          "recordLimit" : -1,  --允许的错误记录数（-1为不允许出错）
          "byteSpeed" : -1,  --迁移速度（byte）限制
          "batchSize" : 2048  --批量写入数据条数
        }
      },
      "dataTypeConfig" : {  --数据类型设置，当前只支持全局设置，及当前的设置会应用到所有的表，如果不设置，会使用默认映射规则
        "dataTypeMappers" : [ {  --数据类型映射规则
          "srcDataType" : "VARCHAR2",
          "srcDataLength" : 256,
          "tarDataType" : "varchar",
          "tarDataLength" : 500
        } ]
      },
      "tableSpaceConfig" : {  --表空间设置
        "tableSpaceMappers" : [ {  --表空间映射规则
          "tableSpaceName" : "TEST_TBS",
          "srcPath" : "/home/oracle/app/oracle/oradata/helowin/test_tbs.dbf",
          "tarPath" : "/home/omm/tbs",
          "spcMaxSize" : "1048576K"
        } ]
      },
      "keywordsConfig" : { }  --关键字设置
    }
  
    ```
  * 快照（snapshot）json文件说明
    ```
    {
      "id" : "1",
      "dsId" : "src",
      "dbType" : "Oracle",
      "status" : "valid",
      "createdTime" : 1692162804516,
      "info" : "{\"SCN\":1935737}"
    }
    ```