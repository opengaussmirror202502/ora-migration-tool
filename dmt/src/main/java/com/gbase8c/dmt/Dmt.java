package com.gbase8c.dmt;

import com.gbase8c.dmt.config.DmtConfig;
import com.gbase8c.dmt.service.TaskService;
import org.apache.commons.cli.*;

public class Dmt {

    public static void main(String[] args) throws ParseException {
        Options options = new Options();

        options.addOption("workdir", true, "work dir.");
        options.addOption("taskid", true, "task id.");
        options.addOption("action", true, "action.");
        options.addOption("tables", true, "tables.");

        DefaultParser parser = new DefaultParser();
        CommandLine cl = parser.parse(options, args);

        String workdir = cl.getOptionValue("workdir");
        String taskId = cl.getOptionValue("taskid");
        String action = cl.getOptionValue("action");
        String tables = cl.getOptionValue("tables");

        DmtConfig dmtConfig = DmtConfig.read(workdir, taskId);
        TaskService taskService = new TaskService(dmtConfig);
        if (!"rbt".equalsIgnoreCase(action)) {
            taskService.start();
        } else {
            taskService.resumeBt(tables);
        }
    }
}
