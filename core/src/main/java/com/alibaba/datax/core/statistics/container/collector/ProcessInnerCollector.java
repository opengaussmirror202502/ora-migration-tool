package com.alibaba.datax.core.statistics.container.collector;

import com.alibaba.datax.core.statistics.communication.Communication;
import com.alibaba.datax.dataxservice.face.domain.enums.State;

import java.util.Map;

public class ProcessInnerCollector extends AbstractCollector {

    public ProcessInnerCollector(Map<Integer, Communication> taskGroupCommunicationMap) {
        super(taskGroupCommunicationMap);
    }

    @Override
    public Communication collectFromTaskGroup() {
        Communication communication = new Communication();
        communication.setState(State.SUCCEEDED);

        for (Communication taskGroupCommunication :
                taskGroupCommunicationMap.values()) {
            communication.mergeFrom(taskGroupCommunication);
        }

        return communication;
    }

}
