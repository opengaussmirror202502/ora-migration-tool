package com.gbase8c.dmt.model.migration.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.Set;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class RoleDto extends MigrationObject {
    //数据源
    private String ds;
    //角色系统权限集合
    private SysPrivDto sysPrivDto;
    //角色库级权限集合
    private List<DatabasePrivDto> databasePrivDtos;
    //角色表权限集合
    private List<TablePrivDto> tablePrivDtos;
    //角色列权限集合
    private List<ColumnPrivDto> columnPrivDtos;
    //
    private List<Set<String>> allNames;
    //角色sql
    private String tarSql;

//    //用户集合
//    private List<UserDto> userDtos;

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    //管理权限默认都为No
    public static class SysPrivDto{
        private String superUserPriv = "N";
        private String inheritPriv = "N" ;
        private String creatDbPriv = "N" ;
        private String creatRolePriv = "N" ;
        private String loginPriv = "N";
        private String replicationPriv = "N";
        private String bypassRlsPriv = "N";
    }

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DatabasePrivDto {
        private String schemaName;
        private String grantor;
        private String grantee;

        //为了接收mysql库管理权限
        private String createPriv = "N";
        private String selectPriv = "N";
        private String insertPriv = "N";
        private String updatePriv = "N";
        private String deletePriv= "N";
        private String referencesPriv= "N";
        private String executePriv = "N";
        private String triggerPriv = "N";

        private String privileges;
    }

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TablePrivDto {
        private String schemaName;
        private String tableName;
        private String grantor;
        private String grantee;

        private String selectPriv = "N";
        private String insertPriv = "N";
        private String updatePriv = "N";
        private String deletePriv = "N";
        private String referencesPriv = "N";
        private String truncatePriv = "N";
        private String triggerPriv = "N";

        //Mysql接到的权限是一个String，逗号分隔
        private String privileges;
    }

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ColumnPrivDto {
        private String schemaName;
        private String tableName;
        private String columnName;

        private String grantor;
        private String grantee;

        private String selectPriv = "N";
        private String insertPriv = "N";
        private String updatePriv = "N";
        private String deletePriv = "N";
        private String referencesPriv = "N";

        //Mysql接到的权限是一个String，逗号分隔
        private String privileges;
    }


}
