package com.gbase8c.dmt.db.meta;

import com.gbase8c.dmt.model.migration.config.Snapshot;
import org.apache.commons.dbutils.ResultSetHandler;

import java.util.List;

public interface Meta {

    List<String> getDataTypes();
    Integer toSqlType(String dataType);
    String toDataType(Integer sqlType);
    String wrap(String name, boolean preserveCase);
    List<String> keywords();

    String characterSet(String database);

    <T> T query(String sql, ResultSetHandler<T> rsh, Object... params);
    int execute(String sql, Object... params);

    Snapshot snapshot();
    boolean valid(Snapshot snapshot);
}
