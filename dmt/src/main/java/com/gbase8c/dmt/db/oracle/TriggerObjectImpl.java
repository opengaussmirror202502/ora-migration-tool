package com.gbase8c.dmt.db.oracle;

import com.gbase8c.dmt.db.object.TriggerObject;
import com.gbase8c.dmt.model.migration.dto.DataSourceDto;
import com.gbase8c.dmt.model.migration.dto.TriggerDto;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;

import java.util.List;
import java.util.Map;

@Slf4j
public class TriggerObjectImpl extends MetaImpl implements TriggerObject {

    public TriggerObjectImpl(DataSourceDto dataSourceDto) {
        super(dataSourceDto);
    }

    @Override
    public List<String> getNames(Map<String, Object> params) {
        String schema = (String) params.get("schema");
        List<String>  triggerNames = Lists.newArrayList();
        StringBuilder sql = new StringBuilder("SELECT trigger_name FROM all_triggers WHERE owner = ?");
        triggerNames = query(sql.toString(), new ColumnListHandler<String>(), schema);
        return triggerNames;
    }

    @Override
    public TriggerDto get(String name, Map<String, Object> params) {
        String triggerDtoSql = "SELECT owner AS schema,trigger_name as name,trigger_type as oraTriType,triggering_event as triggeringEvent," +
                "table_owner as eventObjectSchema,table_name as eventObjectTable,description FROM all_triggers WHERE owner = ? AND trigger_name = ?";
        String triggerBodysql = "SELECT trigger_body as srcTriggerBody FROM all_triggers WHERE owner = ? AND trigger_name = ?";
        TriggerDto dto = null;
        String schema = (String) params.get("schema");
        //获得触发器信息
        dto = query(triggerDtoSql, new BeanHandler<>(TriggerDto.class), schema, name);
        List<String> src = Lists.newArrayList();
        String srcTriggerBody = null;
        src = query(triggerBodysql,new ColumnListHandler<>(),schema,name);
        if (CollectionUtils.isNotEmpty(src)){
            srcTriggerBody = src.get(0);
        }

        String type = dto.getOraTriType();
        String timing = null;
        String orientation = null;
        String[] typeList = type.split(" ");
        System.out.println(typeList);
        if (typeList.length != 0){
            timing = typeList[0];
            orientation = typeList[1];
        }

        dto.setActionTiming(timing);
        dto.setActionOrientation(orientation);
        dto.setName(dto.getName());
        dto.setSrcTriggerBody(srcTriggerBody);
        return dto;
    }

    @Override
    public TriggerDto convert(TriggerDto triggerDto, Map<String, Object> params) {
        Boolean convertible = Boolean.TRUE;

        triggerDto.setConvertible(convertible);
        if (triggerDto.getConvertible()){
            triggerDto.setConvertMsg("已完成");
        } else {
            triggerDto.setConvertMsg("失败");
        }

        return triggerDto;
    }

    @Override
    public String sql(TriggerDto triggerDto, Map<String, Object> params) {
        return null;
    }

    @Override
    public TriggerDto getTriggerDto(String schema, TriggerDto triggerDto) {
        String triggerDtoSql = "SELECT owner AS schema,trigger_name as name,trigger_type as oraTriType,triggering_event as triggeringEvent," +
                "table_owner as eventObjectSchema,table_name as eventObjectTable,description FROM all_triggers WHERE owner = ? AND trigger_name = ?";
        String triggerBodysql = "SELECT trigger_body as srcTriggerBody FROM all_triggers WHERE owner = ? AND trigger_name = ?";
        TriggerDto dto = null;
        //获得触发器信息
        dto = query(triggerDtoSql, new BeanHandler<>(TriggerDto.class), schema, triggerDto.getName());
        List<String> src = Lists.newArrayList();
        String srcTriggerBody = null;
        src = query(triggerBodysql,new ColumnListHandler<>(),schema,triggerDto.getName());
        if (CollectionUtils.isNotEmpty(src)){
            srcTriggerBody = src.get(0);
        }

        String type = dto.getOraTriType();
        String timing = null;
        String orientation = null;
        String[] typeList = type.split(" ");
        System.out.println(typeList);
        if (typeList.length != 0){
            timing = typeList[0];
            orientation = typeList[2];
        }

        dto.setActionTiming(timing);
        dto.setActionOrientation(orientation);
        dto.setName(dto.getName());
        dto.setSrcTriggerBody(srcTriggerBody);
        return dto;
    }

    @Override
    public List<String> triggerSql(TriggerDto triggerDto) {
        return Lists.newArrayList();
    }
}
