package com.gbase8c.dmt.db.opengauss;

import com.gbase8c.dmt.db.object.ColumnObject;
import com.gbase8c.dmt.model.migration.dto.ColumnType;
import com.gbase8c.dmt.model.migration.dto.DataSourceDto;
import com.gbase8c.dmt.model.migration.dto.TableDto;
import com.google.common.collect.Sets;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class ColumnObjectImpl extends MetaImpl implements ColumnObject {

    public ColumnObjectImpl(DataSourceDto dataSourceDto) {
        super(dataSourceDto);
    }

    @Override
    public List<TableDto.ColumnDto> columnDtos(String schema, String tableName) {
        return null;
    }

    @Override
    public List<ColumnType> columnTypes(String schema, String type, List<String> tableNames) {
        return null;
    }

    private static Set<String> dts = Sets.newHashSet(
            "char", "character", "nchar", "bpchar",
            "varchar", "varchar2", "character varying");
    private static int max_length = 10 * 1024 * 1024;
    @Override
    /**
     * char(n) character(n) nchar(n) 定长字符串，不足补空格。n是指字节长度，如不带精度n，默认精度为1。 最大为10MB。
     * varchar(n) varchar2(n) character varying(n) 变长字符串。PG兼容模式下，n是字符长度。其他兼容模式下，n是指字节长度。 最大为10MB。
     * nvarchar(n) nvarchar2(n) 变长字符串。n是指字符长度。 最大为10MB。
     * text 变长字符串。最大为1GB-1
     * clob 文本大对象。最大为4GB-1
     */
    public ColumnType toColumnType(ColumnType srcColumnType) {
        String tarDataType = toDataType(srcColumnType.getSqlType());
        ColumnType tarColumnType = new ColumnType();
        if (StringUtils.isNotBlank(tarDataType)) {
            tarColumnType.setDataType(tarDataType);
        } else {
            // todo
            tarColumnType.setDataType(srcColumnType.getDataType());
        }
        tarColumnType.setDataLength(srcColumnType.getDataLength());
        tarColumnType.setDataPrecision(srcColumnType.getDataPrecision());
        tarColumnType.setDataScale(srcColumnType.getDataScale());
        return tarColumnType;
    }

    @Override
    public List<String> getNames(Map<String, Object> params) {
        return null;
    }

    @Override
    public TableDto.ColumnDto get(String name, Map<String, Object> params) {
        return null;
    }

    @Override
    public TableDto.ColumnDto convert(TableDto.ColumnDto columnDto, Map<String, Object> params) {
        return null;
    }

    @Override
    public String sql(TableDto.ColumnDto columnDto, Map<String, Object> params) {
        return null;
    }
}
