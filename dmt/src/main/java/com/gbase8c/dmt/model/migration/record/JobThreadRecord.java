package com.gbase8c.dmt.model.migration.record;

import com.gbase8c.dmt.model.migration.dto.MigrationObject;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JobThreadRecord {

    /**
     * 线程执行结果
     */
    Boolean flag;

    /**
     * 模式名
     */
    String schemaName;

    /**
     * 执行错误对象
     */
    MigrationObject dbObject;

    /**
     * 执行错误日志
     */
    String errorLog;
}
