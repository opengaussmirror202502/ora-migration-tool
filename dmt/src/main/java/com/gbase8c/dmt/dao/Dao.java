package com.gbase8c.dmt.dao;

import java.util.List;

public interface Dao<T> {

    T get(String id);

    List<T> list();

    void delete(String id);

    void delete(T t);

    T save(T t);

    void update(T t);
}
