package com.gbase8c.dmt.common.exception;

public class DmtException extends Exception {

    public DmtException(){
        super();
    }
    public DmtException(String msg){
        super(msg);
    }

    public DmtException(String msg, Throwable cause){
        super(msg, cause);
    }

    public DmtException(Throwable cause) {
        super(cause.getMessage(), cause);
    }
}
