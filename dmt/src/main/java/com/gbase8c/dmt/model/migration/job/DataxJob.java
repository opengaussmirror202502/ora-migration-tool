package com.gbase8c.dmt.model.migration.job;

import com.gbase8c.dmt.model.migration.dto.DataSourceDto;
import com.gbase8c.dmt.model.migration.dto.MigrationObject;
import com.google.common.collect.Lists;
import lombok.*;
import lombok.experimental.SuperBuilder;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class DataxJob extends MigrationObject {
    private String taskId;
    private String tableJobName;
    private Long jobId;
    private DataSourceDto src;
    private DataSourceDto tar;
    private String json;

    @Override
    public List<String> getContents() {
        if (CollectionUtils.isEmpty(this.contents)){
            this.contents = Lists.newArrayList(json);
        }
        return this.contents;
    }
}
