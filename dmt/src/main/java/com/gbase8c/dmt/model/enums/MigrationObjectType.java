package com.gbase8c.dmt.model.enums;

public enum MigrationObjectType {
    SCHEMA("schema", "模式", "sql"),
    SEQUENCE("sequence", "序列", "sql"),
    TABLESPACE("tablespace", "表空间", "sql"),
    TABLE("table", "表结构", "sql"),
    DATA("data", "数据", "bash"),
    PK("pk", "主键", "sql"),
    INDEX("index", "索引", "sql"),
    UNIQUE("unique", "唯一", "sql"),
    FK("fk", "外键", "sql"),
    CHECK("check", "检查", "sql"),
    VIEW("view", "视图", "sql"),
    FUNCTION("function", "函数", "sql"),
    TRIGGER("trigger", "触发器", "sql"),
    SYNONYM("synonym", "同义词", "sql");

    private String name;

    private String type;

    private String executeType;

    MigrationObjectType(String name, String type, String executeType){
        this.name = name;
        this.type = type;
        this.executeType = executeType;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public String getExecuteType() {
        return executeType;
    }

}
