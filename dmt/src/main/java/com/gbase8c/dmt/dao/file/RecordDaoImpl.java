package com.gbase8c.dmt.dao.file;

import com.gbase8c.dmt.config.DmtConfig;
import com.gbase8c.dmt.dao.RecordDao;
import com.gbase8c.dmt.model.migration.record.MigrationRecordDto;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.List;
import java.util.stream.Collectors;


public class RecordDaoImpl extends BasicDao<MigrationRecordDto> implements RecordDao {

    private DmtConfig dmtConfig;

    public RecordDaoImpl(DmtConfig dmtConfig) {
        super();
        this.dmtConfig = dmtConfig;
        mkdirs(path());
    }

    @Override
    public String path() {
        return dmtConfig.taskDir() + File.separator + "record";
    }
    
    @Override
    public File file(MigrationRecordDto migrationRecordDto) {
        String filepath = StringUtils.join(new String[] {path(), migrationRecordDto.getId() + ".json"}, File.separator);
        return new File(filepath);
    }

    @Override
    public void preSave(String id, MigrationRecordDto migrationRecordDto) {
        migrationRecordDto.setId(id);
    }

    @Override
    public List<MigrationRecordDto> list(String taskId) {
        List<MigrationRecordDto> dtos = list();
        return dtos.stream()
                .filter(dto -> dto.getTaskId().equalsIgnoreCase(taskId))
                .collect(Collectors.toList());
    }

    @Override
    public void delete(MigrationRecordDto migrationRecordDto) {
        delete(migrationRecordDto.getId());
    }

    @Override
    public void update(MigrationRecordDto migrationRecordDto) {

    }
}
