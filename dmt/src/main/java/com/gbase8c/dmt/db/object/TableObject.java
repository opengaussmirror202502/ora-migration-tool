package com.gbase8c.dmt.db.object;

import com.gbase8c.dmt.model.migration.dto.TableDto;
import com.gbase8c.dmt.model.migration.dto.TableTypeMapperDto;

import java.math.BigDecimal;
import java.util.List;

public interface TableObject extends DbObject<TableDto> {

    List<String> tableSql(TableDto tableDto);

    List<TableTypeMapperDto> tableTypeMapper(String schema);

    BigDecimal dataSize(String schema, String tableName);
}
