package com.gbase8c.dmt.model.migration.dto;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TurningHistoryDto {
    private String sql;
//    private Date eventTime;
}
