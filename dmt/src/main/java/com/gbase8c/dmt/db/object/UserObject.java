package com.gbase8c.dmt.db.object;

import com.gbase8c.dmt.model.migration.dto.UserDto;

public interface UserObject extends DbObject<UserDto> {
}
