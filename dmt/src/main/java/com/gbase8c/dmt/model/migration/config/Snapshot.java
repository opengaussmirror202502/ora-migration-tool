package com.gbase8c.dmt.model.migration.config;

import com.gbase8c.dmt.model.enums.DbType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Snapshot {
    // id
    private String id;
    // 数据源id
    private String dsId;
    private DbType dbType;
    private Status status;
    // 创建时间
    private Date createdTime;
    private String note;
    private String info;

    public enum Status {
        valid, invalid;
    }

}
