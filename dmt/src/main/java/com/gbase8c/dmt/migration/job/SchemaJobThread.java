package com.gbase8c.dmt.migration.job;

import com.gbase8c.dmt.common.util.Exceptions;
import com.gbase8c.dmt.common.util.Reflections;
import com.gbase8c.dmt.migration.MoLogger;
import com.gbase8c.dmt.model.enums.MigrationObjectType;
import com.gbase8c.dmt.model.migration.dto.MigrationObject;
import com.gbase8c.dmt.model.migration.record.JobThreadRecord;
import com.gbase8c.dmt.model.enums.Status;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.QueryRunner;

import javax.sql.DataSource;
import java.sql.SQLException;

@Slf4j
public class SchemaJobThread extends AbstractJobThread{

    public SchemaJobThread(MigrationObjectType type, MigrationObject dbObject, DataSource tarDataSource, MoLogger moLogger) {
        super(type, dbObject, tarDataSource, moLogger);
    }

    @Override
    public JobThreadRecord call() throws Exception {
        JobThreadRecord jobThreadRecord = new JobThreadRecord();
        String sql = (String) Reflections.invokeGetter(dbObject, "tarSql");
        String schema = (String) Reflections.invokeGetter(dbObject, "tarSchemaName");
        QueryRunner queryRunner = new QueryRunner(this.tarDataSource);
        jobThreadRecord.setSchemaName(schema);
        try {
            //todo:添加convert转换成功与否判断
            int execute = queryRunner.execute(sql);
            log.info("sql:{} \n 执行结果: {}",sql,execute);
            jobThreadRecord.setFlag(true);
            dbObject.setStatus(Status.Finished);
        }catch (SQLException exception) {
            jobThreadRecord.setFlag(false);
            log.error(exception.getMessage(),exception);
            String errorLog = Exceptions.getStackTraceAsString(exception);
            Reflections.invokeSetter(dbObject,"msg",errorLog);
            jobThreadRecord.setErrorLog(exception.getMessage());
//            exception.printStackTrace();
        }
        jobThreadRecord.setDbObject(dbObject);
        return jobThreadRecord;
    }
}
