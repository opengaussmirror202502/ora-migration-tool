package com.gbase8c.dmt.model.migration.report;

import com.gbase8c.dmt.model.migration.dto.TransObjDto;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SchemaStats {

    SchemaTypes schemaType;
    Long srcSchemaNum;
    Long tarSchemaNum;
    String percent;

    TransObjDto transObjDto;

    public enum SchemaTypes{
        TABLE,VIEW,FUNCTION,CONSTRAINT,SEQUENCE,SYNONYM,TRIGGER,TYPE,INDEX,USER,ROLE,TABLESPACE;
    }
}
