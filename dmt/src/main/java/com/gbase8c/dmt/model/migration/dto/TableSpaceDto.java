package com.gbase8c.dmt.model.migration.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class TableSpaceDto extends MigrationObject {

    private Boolean initializable;
    /**
     * 源库表空间文件夹
     */
    private String srcPath;

    /**
     * 目标库表空间文件夹
     */
    private String tarPath;

    //表空间sql
    private String tarSql;

    //表空间所有者（owner）
    private String useName;

    //
    private String relative;

    //
    private String spcMaxSize;

    //
    private String spcOptions;

}
