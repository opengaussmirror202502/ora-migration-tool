package com.gbase8c.dmt.model.migration.dto;

import lombok.*;

import java.math.BigDecimal;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ColumnType {

    private String dataType;
    private Integer sqlType;
    // 长度
    private BigDecimal dataLength;
    private Long length;
    // 精度
    private Integer dataPrecision;
    // 小数点位数
    private Integer dataScale;

}
