package com.gbase8c.dmt.model.migration.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class SequenceDto extends MigrationObject {

    // 序列以多大的步数增长
    private String incrementBy;

    // 序列最小值
    private String minValue;

    // 序列最大值
    private String maxValue;

    // 序列以多大开始
    private String startWith;

    // 序列是否设置缓存
    private Boolean cache;

    // 如果设置缓存，缓存大小
    private String cacheSize;

    // 是否可循环
    private String cycle;

    // 序列与一个表的指定字段进行关联
    private String ownedBy;

    // 序列last_number
    private String lastNumber;

    //迁移评估报告序列备注信息
    private String tarSql;

}
