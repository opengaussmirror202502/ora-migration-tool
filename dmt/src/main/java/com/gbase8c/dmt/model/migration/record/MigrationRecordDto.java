package com.gbase8c.dmt.model.migration.record;

import com.gbase8c.dmt.model.migration.dto.TableMapper;
import com.gbase8c.dmt.model.enums.MigrationTaskStatus;
import lombok.*;

import java.util.Date;
import java.util.List;
import java.util.Map;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
// 迁移任务记录DTO
public class MigrationRecordDto {

    // 记录ID
    private String id;

    // 任务关联的ID
    private String taskId;

    // 迁移任务状态
    private MigrationTaskStatus migrationTaskStatus;

    // 源数据源
    private String srcDs;

    // 目标数据源
    private String tarDs;

    // 开始时间
    private Date startTime;

    // 结束时间
    private Date endTime;

    // 状态 0/记录中 1/归档
    private int status;

   // 记录不同类型数据库对象的转换执行记录
    private Map<String,List<ExecuteRecord>> schemaRecordsMap;

    // 映射关系
    private List<TableMapper> tableMappers;

}
