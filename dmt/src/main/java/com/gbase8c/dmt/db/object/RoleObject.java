package com.gbase8c.dmt.db.object;

import com.gbase8c.dmt.model.migration.dto.RoleDto;

public interface RoleObject extends DbObject<RoleDto> {
}
