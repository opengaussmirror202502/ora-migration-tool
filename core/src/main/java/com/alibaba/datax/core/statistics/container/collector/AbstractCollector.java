package com.alibaba.datax.core.statistics.container.collector;

import com.alibaba.datax.common.util.Configuration;
import com.alibaba.datax.core.statistics.communication.Communication;
import com.alibaba.datax.core.util.container.CoreConstant;
import com.alibaba.datax.dataxservice.face.domain.enums.State;
import org.apache.commons.lang3.Validate;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public abstract class AbstractCollector {

    protected Map<Integer, Communication> taskCommunicationMap = new ConcurrentHashMap<>();

    protected Map<Integer, Communication> taskGroupCommunicationMap = new ConcurrentHashMap<>();
    private Long jobId;

    public AbstractCollector(Map<Integer, Communication> taskGroupCommunicationMap) {
        this.taskGroupCommunicationMap = taskGroupCommunicationMap;
    }

    public Map<Integer, Communication> getTaskCommunicationMap() {
        return taskCommunicationMap;
    }

    public Map<Integer, Communication> getTaskGroupCommunicationMap() {
        return taskGroupCommunicationMap;
    }

    public Long getJobId() {
        return jobId;
    }

    public void setJobId(Long jobId) {
        this.jobId = jobId;
    }

    public void registerTGCommunication(List<Configuration> taskGroupConfigurationList) {
        for (Configuration config : taskGroupConfigurationList) {
            int taskGroupId = config.getInt(
                    CoreConstant.DATAX_CORE_CONTAINER_TASKGROUP_ID);
            taskGroupCommunicationMap.put(taskGroupId, new Communication());
        }
    }

    public void registerTaskCommunication(List<Configuration> taskConfigurationList) {
        for (Configuration taskConfig : taskConfigurationList) {
            int taskId = taskConfig.getInt(CoreConstant.TASK_ID);
            String state = taskConfig.getString("state");
            Communication communication = new Communication();
            if (State.SUCCEEDED.name().equals(state)) {
                communication.setState(State.SUCCEEDED);
            }
            this.taskCommunicationMap.put(taskId, communication);
        }
    }

    public Communication collectFromTask() {
        Communication communication = new Communication();
        communication.setState(State.SUCCEEDED);

        for (Communication taskCommunication :
                this.taskCommunicationMap.values()) {
            communication.mergeFrom(taskCommunication);
        }

        return communication;
    }

    public abstract Communication collectFromTaskGroup();

    public Map<Integer, Communication> getTGCommunicationMap() {
        return taskGroupCommunicationMap;
    }

    public Communication getTGCommunication(Integer taskGroupId) {
        Validate.isTrue(taskGroupId >= 0, "taskGroupId不能小于0");
        return taskGroupCommunicationMap.get(taskGroupId);
    }

    public Communication getTaskCommunication(Integer taskId) {
        return this.taskCommunicationMap.get(taskId);
    }
}
