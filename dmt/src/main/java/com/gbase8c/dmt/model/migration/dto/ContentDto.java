package com.gbase8c.dmt.model.migration.dto;

import com.gbase8c.dmt.model.enums.DbType;
import lombok.*;

import java.util.Date;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContentDto {
    private String id;
    private String taskName;
    private Date createdTime;
    private String srcName;
    private String srcIp;
    private DbType srcDbType;
    private String tarName;
    private String tarIp;
    private DbType tarDbType;

    private Integer total;
    private Integer succeed;
    private Integer failed;
    private String autoConversionRate;
}
