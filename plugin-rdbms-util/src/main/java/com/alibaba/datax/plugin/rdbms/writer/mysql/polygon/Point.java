package com.alibaba.datax.plugin.rdbms.writer.mysql.polygon;

public class Point implements IPolygon{
    private double x;
    private double y;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }



    @Override
    public String toString() {
        return String.format("(%f,%f)",this.x,this.y);
    }


    @Override
    public String toGBase8cStringValue() {
        return String.format("(%f,%f)",this.x,this.y);
    }
}