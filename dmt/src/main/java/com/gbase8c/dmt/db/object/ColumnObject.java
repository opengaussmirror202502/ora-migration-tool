package com.gbase8c.dmt.db.object;

import com.gbase8c.dmt.model.migration.dto.ColumnType;
import com.gbase8c.dmt.model.migration.dto.TableDto;

import java.util.List;

public interface ColumnObject extends DbObject<TableDto.ColumnDto> {

    List<TableDto.ColumnDto> columnDtos(String schema, String tableName);

    List<ColumnType> columnTypes(String schema, String type, List<String> tableNames);

    ColumnType toColumnType(ColumnType srcColumnType);
}
