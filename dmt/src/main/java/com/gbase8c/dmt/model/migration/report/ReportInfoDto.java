package com.gbase8c.dmt.model.migration.report;

import com.gbase8c.dmt.model.migration.dto.DataSourceDto;
import com.gbase8c.dmt.model.migration.record.MigrationRecordDto;
import lombok.*;

import java.util.Date;
import java.util.List;

@Data
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ReportInfoDto {

    private String taskName;

    private Date createTime;

    private Date startTime;

    private Date endTime;

    private DataSourceDto srcDs;

    private DataSourceDto tarDs;

    private List<MigrationRecordDto> records;
}
