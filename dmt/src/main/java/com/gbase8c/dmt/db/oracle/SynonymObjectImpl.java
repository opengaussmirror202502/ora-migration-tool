package com.gbase8c.dmt.db.oracle;

import com.gbase8c.dmt.db.object.SynonymObject;
import com.gbase8c.dmt.model.migration.dto.DataSourceDto;
import com.gbase8c.dmt.model.migration.dto.SynonymDto;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.handlers.BeanHandler;
import org.apache.commons.dbutils.handlers.BeanListHandler;
import org.apache.commons.dbutils.handlers.ColumnListHandler;

import java.util.List;
import java.util.Map;

@Slf4j
public class SynonymObjectImpl extends MetaImpl implements SynonymObject {

    public SynonymObjectImpl(DataSourceDto dataSourceDto) {
        super(dataSourceDto);
    }

    @Override
    public List<String> getNames(Map<String, Object> params) {
        String schema = (String) params.get("schema");
        List<String>  synNames = Lists.newArrayList();
        List<String>  pubSynNames = Lists.newArrayList();
        List<String>  allSynNames = Lists.newArrayList();
//        String userNameSql = "select username from dba_users where TO_NUMBER(TO_CHAR(created, 'yyyymmdd')) not in (select min(TO_NUMBER(TO_CHAR(created, 'yyyymmdd'))) from dba_users)";
//        List<String> userNames = query(userNameSql,new ColumnListHandler<>());
//        List<String> userNames1 = Lists.newArrayList();
//        for (String userName : userNames){
//            userName = "\'" + userName + "\'";
//            userNames1.add(userName);
//        }
//        String users = StringUtils.join(userNames1,",");
//        StringBuilder sql = new StringBuilder("SELECT synonym_name FROM all_synonyms WHERE owner = ?");
        StringBuilder sql = new StringBuilder("SELECT synonym_name FROM all_synonyms WHERE owner = ?");
        StringBuilder pubSql = new StringBuilder("SELECT synonym_name FROM all_synonyms WHERE owner = 'PUBLIC' AND table_owner = ?");
//        sql.append(users).append(")");
        synNames = query(sql.toString(), new ColumnListHandler<String>(), schema);
        pubSynNames = query(pubSql.toString(), new ColumnListHandler<String>(), schema);
        allSynNames.addAll(synNames);
        allSynNames.addAll(pubSynNames);
        return allSynNames;
    }

    @Override
    public SynonymDto get(String name, Map<String, Object> params) {
        String schema = (String) params.get("schema");
        String sql = "SELECT owner as schema, synonym_name as name, table_owner as objectOwner, table_name as synObjName FROM all_synonyms WHERE owner = ? and synonym_name = ?";
        SynonymDto dto = query(sql, new BeanHandler<>(SynonymDto.class), schema, name);
        if ( null == dto) {
            String publicSql = "SELECT owner as schema, synonym_name as name, table_owner as objectOwner, table_name as synObjName FROM all_synonyms WHERE owner = 'PUBLIC' and synonym_name = ?";
            dto = query(publicSql, new BeanHandler<>(SynonymDto.class), name);
        }
        return dto;
    }

    @Override
    public SynonymDto convert(SynonymDto synonymDto, Map<String, Object> params) {
        Boolean convertible = Boolean.TRUE;
        synonymDto.setConvertible(convertible);
        if (synonymDto.getConvertible()){
            synonymDto.setConvertMsg("已完成");
        } else {
            synonymDto.setConvertMsg("失败");
        }

        return synonymDto;
    }

    @Override
    public String sql(SynonymDto synonymDto, Map<String, Object> params) {
        String synonymName = synonymDto.getName().toLowerCase();
        String schema = synonymDto.getName().toLowerCase();
        String synObjName = synonymDto.getSynObjName().toLowerCase();
        ////创建序列
        //create synonym t_class for test_dmt.t_class
        StringBuilder stringBuilder = new StringBuilder();
        //stringBuilder.append("drop synonym ").append(schema).append(".").append(synonymName).append(";");
        stringBuilder.append("create synonym ").append(schema).append(".").append(synonymName).append(" for ")
                .append(schema).append(".").append(synObjName);
        log.info(stringBuilder.toString());
        return stringBuilder.toString();
    }

    @Override
    public List<SynonymDto> getSynonymDtos(String schema) {
        List<SynonymDto>  synDtos = Lists.newArrayList();
        List<SynonymDto>  pubSynDtos = Lists.newArrayList();
        List<SynonymDto>  allSynDtos = Lists.newArrayList();
//        String userNameSql = "select username from dba_users where TO_NUMBER(TO_CHAR(created, 'yyyymmdd')) not in (select min(TO_NUMBER(TO_CHAR(created, 'yyyymmdd'))) from dba_users)";
//        List<String> userNames = query(userNameSql,new ColumnListHandler<>());
//        List<String> userNames1 = Lists.newArrayList();
//        for (String userName : userNames){
//            userName = "\'" + userName + "\'";
//            userNames1.add(userName);
//        }
//        String users = StringUtils.join(userNames1,",");
//        StringBuilder sql = new StringBuilder("SELECT synonym_name FROM all_synonyms WHERE owner = ?");
        StringBuilder sql = new StringBuilder("SELECT owner as schema, synonym_name as name FROM all_synonyms WHERE owner = ?");
        StringBuilder pubSql = new StringBuilder("SELECT owner as schema, synonym_name as name FROM all_synonyms WHERE owner = 'PUBLIC' AND table_owner = ?");
//        sql.append(users).append(")");
        synDtos = query(sql.toString(), new BeanListHandler<>(SynonymDto.class),schema);
        pubSynDtos = query(pubSql.toString(), new BeanListHandler<>(SynonymDto.class),schema);
        allSynDtos.addAll(synDtos);
        allSynDtos.addAll(pubSynDtos);
        return allSynDtos;
    }
}
