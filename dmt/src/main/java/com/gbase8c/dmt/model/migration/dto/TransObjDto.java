package com.gbase8c.dmt.model.migration.dto;

import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TransObjDto {

    private List<SchemaDto> schemaDtos;
    private UserPrivDto userPrivDto;
    private List<TableSpaceDto> tableSpaceDtos;

}
