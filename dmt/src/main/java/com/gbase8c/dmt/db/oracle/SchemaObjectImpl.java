package com.gbase8c.dmt.db.oracle;

import com.gbase8c.dmt.db.object.SchemaObject;

import com.gbase8c.dmt.model.migration.dto.DataSourceDto;
import com.gbase8c.dmt.model.migration.dto.SchemaDto;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.dbutils.handlers.ColumnListHandler;

import java.util.List;
import java.util.Map;

@Slf4j
public class SchemaObjectImpl extends MetaImpl implements SchemaObject {

    public SchemaObjectImpl(DataSourceDto dataSourceDto) {
        super(dataSourceDto);
    }

    @Override
    public List<String> getNames(Map<String, Object> params) {
        String sql = "select username from dba_users where TO_NUMBER(TO_CHAR(created, 'yyyymmdd')) not in (select min(TO_NUMBER(TO_CHAR(created, 'yyyymmdd'))) from dba_users)";
        List<String> schemaNames = query(sql, new ColumnListHandler<String>());
        return schemaNames;
    }

    @Override
    public SchemaDto get(String name, Map<String, Object> params) {
        return null;
    }

    @Override
    public SchemaDto convert(SchemaDto schemaDto, Map<String, Object> params) {
        return null;
    }

    @Override
    public String sql(SchemaDto schemaDto, Map<String, Object> params) {
        return null;
    }

    @Override
    public List<String> getNames(Map<String, Object> params, boolean includeSystemSchemas) {
        String sql = null;
        if (includeSystemSchemas) {
            sql = "select username from dba_users";
        } else {
            sql = "select username from dba_users where TO_NUMBER(TO_CHAR(created, 'yyyymmdd')) not in (select min(TO_NUMBER(TO_CHAR(created, 'yyyymmdd'))) from dba_users)";
        }
        List<String> schemaNames = query(sql, new ColumnListHandler<String>());
        return schemaNames;
    }
}
