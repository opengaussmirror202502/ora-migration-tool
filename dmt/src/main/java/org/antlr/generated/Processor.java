package org.antlr.generated;

import com.gbase8c.dmt.model.migration.dto.FunctionDto;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;

public class Processor {

    public static FunctionDto getFunctionDto(String functionStr) {
        CharStream input = CharStreams.fromString(functionStr);

        FunctionLexer lexer = new FunctionLexer(input);
        CommonTokenStream tokens = new CommonTokenStream(lexer);
        FunctionParser parser = new FunctionParser(tokens);

        parser.removeErrorListeners();
        parser.addErrorListener(new FunctionErrorListener());

        ParseTree tree = parser.prog(); // parse

        FunctionVisitorImpl visitor = new FunctionVisitorImpl();
        visitor.visit(tree);

        FunctionDto dto = visitor.getFunctionDto();

        return dto;
    }

}
