package com.gbase8c.dmt.model.migration.config;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gbase8c.dmt.common.util.JsonMapper;
import com.gbase8c.dmt.model.migration.dto.DataSourceDto;
import com.gbase8c.dmt.model.enums.MigrationTaskStatus;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Task {
    // 任务id
    private String id;
    // 任务名称
    private String name;
    // 创建时间
    private Date createdTime;
    // 任务状态
    private MigrationTaskStatus status;
    //源数据库
    private String src;
    @JsonIgnore
    private DataSourceDto srcDs;

    //目标数据库
    private String tar;
    @JsonIgnore
    private DataSourceDto tarDs;

    private String snapshotId;

    // 模式配置
    private SchemaConfig schemaConfig;

    private MigrateConfig migrateConfig;
    private DataTypeConfig dataTypeConfig;
    private TableSpaceConfig tableSpaceConfig;
    private TableTypeConfig tableTypeConfig;
    private KeywordsConfig keywordsConfig;

    // 最近一次taskInfo
    private String taskInfoId;

    @Override
    public String toString() {
        Task taskInfo = Task.builder()
                .id(id)
                .name(name)
                .src(src)
                .tar(tar)
                .schemaConfig(schemaConfig)
                .migrateConfig(migrateConfig)
                .dataTypeConfig(dataTypeConfig)
                .tableSpaceConfig(tableSpaceConfig)
                .tableTypeConfig(tableTypeConfig)
                .keywordsConfig(keywordsConfig)
                .build();
        return JsonMapper.nonEmptyMapper().toJson(taskInfo);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return this.toString().equals(task.toString());
    }
}
