package com.gbase8c.dmt.dao.file;

import com.gbase8c.dmt.config.DmtConfig;
import com.gbase8c.dmt.dao.TaskDao;
import com.gbase8c.dmt.model.migration.config.Task;
import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.util.Date;

public class TaskDaoImpl extends BasicDao<Task> implements TaskDao {

    private DmtConfig dmtConfig;

    public TaskDaoImpl(DmtConfig dmtConfig) {
        super();
        this.dmtConfig = dmtConfig;
        mkdirs(path());
    }

    @Override
    public String path() {
        return dmtConfig.taskDir() + File.separator + "task";
    }

    @Override
    public File file(Task task) {
        String filepath = StringUtils.join(new String[] {path(), task.getId() + ".json"}, File.separator);
        return new File(filepath);
    }

    @Override
    public void preSave(String id, Task task) {
        task.setId(id);
        task.setSrcDs(null);
        task.setTarDs(null);
        task.setCreatedTime(new Date());
    }

    @Override
    public void delete(Task task) {
        delete(task.getId());
    }

}
