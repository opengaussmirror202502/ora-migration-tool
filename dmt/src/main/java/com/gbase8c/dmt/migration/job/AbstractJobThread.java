package com.gbase8c.dmt.migration.job;

import com.gbase8c.dmt.migration.MoLogger;
import com.gbase8c.dmt.model.enums.MigrationObjectType;
import com.gbase8c.dmt.model.migration.dto.MigrationObject;
import com.gbase8c.dmt.model.migration.record.JobThreadRecord;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import javax.sql.DataSource;
import java.util.concurrent.Callable;

@AllArgsConstructor
@Slf4j
public abstract class AbstractJobThread implements Callable<JobThreadRecord> {

    protected MigrationObjectType type;
    protected MigrationObject dbObject;
    protected DataSource tarDataSource;
    protected MoLogger moLogger;

    @Override
    public JobThreadRecord call() throws Exception {
        log.info(this.type.getType());
        return null;
    }
}
