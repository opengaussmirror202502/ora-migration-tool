package com.gbase8c.dmt.model.migration.wrapper;

import com.gbase8c.dmt.model.migration.config.DataTypeConfig;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public class DataTypeWrapper {

    private Map<String, DataTypeConfig.DataTypeMapper> map = Maps.newHashMap();
    private DataTypeConfig dataTypeConfig;

    public DataTypeWrapper(DataTypeConfig dataTypeConfig) {
        this.dataTypeConfig = dataTypeConfig;
        init();
    }

    public static String key(String dataType,
                             BigDecimal dataLength,
                             Integer dataPrecision,
                             Integer dataScale) {
        return dataType + "-" + dataLength + "-" + dataPrecision + "-" + dataScale;
    }

    private void init() {
        if (ObjectUtils.isNotEmpty(dataTypeConfig)) {
            List<DataTypeConfig.DataTypeMapper> dataTypeMappers = dataTypeConfig.getDataTypeMappers();
            if (CollectionUtils.isNotEmpty(dataTypeMappers)) {
                for (DataTypeConfig.DataTypeMapper dataTypeMapper : dataTypeMappers) {
                    String key = key(dataTypeMapper.getSrcDataType(),
                            dataTypeMapper.getSrcDataLength(),
                            dataTypeMapper.getSrcDataPrecision(),
                            dataTypeMapper.getSrcDataScale());
                    map.put(key, dataTypeMapper);
                }
            }
        }
    }

    public DataTypeConfig.DataTypeMapper dataTypeMapper(String dataType,
                                                        BigDecimal dataLength,
                                                        Integer dataPrecision,
                                                        Integer dataScale) {
        String key = key(dataType, dataLength, dataPrecision, dataScale);
        return map.get(key);
    }
}
