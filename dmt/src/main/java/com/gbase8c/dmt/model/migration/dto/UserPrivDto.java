package com.gbase8c.dmt.model.migration.dto;

import lombok.*;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserPrivDto {
    private List<UserDto> userDtos;
    private List<RoleDto> roleDtos;
}
