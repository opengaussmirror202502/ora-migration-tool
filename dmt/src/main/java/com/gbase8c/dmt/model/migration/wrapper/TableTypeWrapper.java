package com.gbase8c.dmt.model.migration.wrapper;

import com.gbase8c.dmt.model.migration.config.TableTypeConfig;
import com.google.common.collect.Maps;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;

import java.util.List;
import java.util.Map;

public class TableTypeWrapper {

    private Map<String, TableTypeConfig.TableTypeMapper> map = Maps.newHashMap();
    private TableTypeConfig tableTypeConfig;

    public TableTypeWrapper(TableTypeConfig tableTypeConfig) {
        this.tableTypeConfig = tableTypeConfig;
        init();
    }

    private void init() {
        if (ObjectUtils.isNotEmpty(tableTypeConfig)) {
            List<TableTypeConfig.TableTypeMapper> tableTypeMappers = tableTypeConfig.getTableTypeMappers();
            if (CollectionUtils.isNotEmpty(tableTypeMappers)) {
                for (TableTypeConfig.TableTypeMapper tableTypeMapper : tableTypeMappers) {
                    String key = key(tableTypeMapper.getSchema(), tableTypeMapper.getTable());
                    map.put(key, tableTypeMapper);
                }
            }
        }
    }

    public static String key(String schema, String table) {
        return schema + "." + table;
    }

    public TableTypeConfig.TableTypeMapper tableTypeMapper(String schema, String table) {
        String key = key(schema, table);
        return map.get(key);
    }
}
