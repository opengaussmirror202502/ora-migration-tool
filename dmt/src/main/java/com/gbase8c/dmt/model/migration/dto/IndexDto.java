package com.gbase8c.dmt.model.migration.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class IndexDto extends MigrationObject {

    // 表名
    private String tableName;

    // 目标表名
    private String tarTableName;

    // 表模式名
    private String tableOwner;

    // 创建唯一性索引，每次添加数据时检测表中是否有重复值。如果插入或更新的值会引起重复的记录时，将导致报错；
    private boolean indexUnique;

    // 指定创建索引的名称，取值范围：btree、hash、gist、spgist、gin以及brin，缺省为btree
    private String srcMethod;

    // 转换后索引数据结构类型
    private String tarMethod;

    // 创建一个基于该表的一个或多个字段的表达式索引，通常必须写在圆括弧中。如果表达式有函数调用的形式，圆括弧可以省略；
    private String expression;
//        /**
//         * 指定列的排序规则（该列必须是可排列的数据类型）。缺省使用默认的排序规则；
//         */
//        private String collation;
//        /**
//         * 操作符类的名字
//         */
//        private String opclass;
//        /**
//         * ASC和DESC
//         */
//        private String order;
    // 指定索引方法的存储参数
    private String storageParameter;

    // 指定索引的表空间，如果没有声明则使用默认的表空间
    private String tableSpaceName;

    // 部分索引
    private String predicate;

    // 单条查询列名使用
    private String columnName;

    // 接收mysql是否唯一字段
    private Long nonUnique;

    // 列名集合
    private List<IndexColumnDto> indexColumns;

    // 是否迁移
    private Boolean isMigration;

    // 是否迁移至表空间
    private Boolean isToTableSpace;

    // oracle用:索引是否唯一或者非唯一
    private String uniqueness;

    private String tarDsType;

    //对象迁移创建表的sql语句
    private String tarSql;

    //是否是分区索引(opengauss用)
    private Boolean isPartIndex = false;

    //分区索引类型：local/global (opengauss用)
    private String partIndexKind;

    //gin索引使用 indexDef直接定义;
    private String indexDef;

    //分区索引信息
    private List<PartitionInfoDto> partitionInfos;

    //分区索引列
    private String partIndexColumn;

    public boolean isIndexUnique() {
        //oracle操作
        if ("UNIQUE".equals(this.uniqueness)){
            this.indexUnique = true;
        }
        return indexUnique;
    }

    public void setIndexUnique(boolean indexUnique) {
        this.indexUnique = indexUnique;
    }

    @Data
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class IndexColumnDto {

        private String columnName;
        private String tarColumnName;
        private String indexOrder;
        private String columnPosition;
        /**
         指定列的排序规则（该列必须是可排列的数据类型）。缺省使用默认的排序规则；
         **/
        private String collation;

    }

}