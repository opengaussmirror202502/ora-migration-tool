package com.gbase8c.dmt.db.object;

import java.util.List;
import java.util.Map;

public interface DbObject<T> {

    List<String> getNames(Map<String, Object> params);

    T get(String name, Map<String, Object> params);

    T convert(T t, Map<String, Object> params);

    String sql(T t, Map<String, Object> params);

}
