package com.gbase8c.dmt.model.migration.config;

import lombok.*;

import java.util.List;

/**
 * 模式设置
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SchemaConfig {

    private String src;
    private String tar;
    // 是否包含系统模式
    private boolean includeSystemSchemas;
    private List<SchemaMapper> schemaMappers;

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SchemaMapper {
        // 源模式
        private String srcSchemaName;
        // 目标模式
        private String tarSchemaName;

        // 表 视图 序列 函数 存储过程 程序包 同义词 触发器
        private Boolean tableMigrated;
        private Boolean viewMigrated;
        private Boolean sequenceMigrated;
        private Boolean functionMigrated;
        private Boolean procedureMigrated;
        private Boolean packageMigrated;
        private Boolean synonymMigrated;
        private Boolean triggerMigrated;

        // 表设置
        private TableConfig tableConfig;
    }

}
