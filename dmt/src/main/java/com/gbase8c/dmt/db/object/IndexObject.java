package com.gbase8c.dmt.db.object;

import com.gbase8c.dmt.model.migration.dto.IndexDto;

import java.util.List;

public interface IndexObject extends DbObject<IndexDto> {

    List<IndexDto> getIndexDtos(String schema);

    IndexDto getIndexDto(String schema,IndexDto indexDto,List<String> tableSpaceNames);

    List<IndexDto> getIndexDtos(String schema, String table);
}
