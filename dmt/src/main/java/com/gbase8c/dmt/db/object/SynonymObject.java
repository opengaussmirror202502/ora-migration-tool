package com.gbase8c.dmt.db.object;

import com.gbase8c.dmt.model.migration.dto.SynonymDto;

import java.util.List;

public interface SynonymObject extends DbObject<SynonymDto> {
    List<SynonymDto> getSynonymDtos(String schema);
}
