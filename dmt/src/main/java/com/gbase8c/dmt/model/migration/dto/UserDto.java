package com.gbase8c.dmt.model.migration.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;
import java.util.Set;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto extends MigrationObject {

    //是否初始化，数据对象转换成功、失败
    private Boolean initializable;
    //源用户数量
    private Long srcCount;
    //目标用户数量
    private Long tarCount;
    //数据源
    private String ds;
    //用户系统权限集合
    private  SysPrivDto sysPrivDto;
    //用户库级权限集合
    private List<DatabasePrivDto> databasePrivDtos;
    //用户表权限集合
    private List<TablePrivDto> tablePrivDtos;
    //用户列权限集合
    private List<ColumnPrivDto> columnPrivDtos;
    //角色集合
    private List<RoleDto> roleDtos;
    //角色集合
    private List<Set<String>> allNames;

    //迁移评估报告用户角色信息
    private String roles;
    //迁移评估报告用户转化结果
    private String convertMsg;
    //用户sql
    private String tarSql;

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    //管理权限默认都为No
    public static class SysPrivDto{
        private String superUserPriv = "N";
        private String inheritPriv = "N" ;
        private String creatDbPriv = "N" ;
        private String creatRolePriv = "N" ;
        private String loginPriv = "Y";
        private String replicationPriv = "N";
        private String bypassRlsPriv = "N";
    }

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class DatabasePrivDto {
        private String schemaName;
        private String grantor;
        private String grantee;

        //为了接收mysql库管理权限
        private String createPriv = "N";
        private String selectPriv = "N";
        private String insertPriv = "N";
        private String updatePriv= "N";
        private String deletePriv= "N";
        private String referencesPriv= "N";
        private String executePriv= "N";
        private String triggerPriv= "N";

        private String privileges;
    }

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class TablePrivDto {
        private String schemaName;
        private String tableName;
        private String grantor;
        private String grantee;

        private String selectPriv= "N";
        private String insertPriv= "N";
        private String updatePriv= "N";
        private String deletePriv= "N";
        private String referencesPriv= "N";
        private String truncatePriv= "N";
        private String triggerPriv= "N";

        //Mysql接到的权限是一个String，逗号分隔
        private String privileges;
    }

    @Getter
    @Setter
    @Builder
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ColumnPrivDto {
        private String schemaName;
        private String tableName;
        private String columnName;

        private String grantor;
        private String grantee;

        private String selectPriv = "N";
        private String insertPriv = "N";
        private String updatePriv = "N";
        private String deletePriv = "N";
        private String referencesPriv = "N";

        //Mysql接到的权限是一个String，逗号分隔
        private String privileges;
    }


}
