package com.gbase8c.dmt.model.migration.dto;

import lombok.*;

import java.util.List;

@Setter
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
// 迁移信息
public class MigrationInfo {

    private String id;

    private String name;

    private String src;

    private DataSourceDto srcDs;

    private String tar;

    private DataSourceDto tarDs;

    private List<TableMapper> mappers;

}
