package com.gbase8c.dmt.model.migration.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class  ViewDto extends MigrationObject {

    //数据源
    private String ds;
    //源视图定义
    private String srcDefinition;
    //视图列集合
    private List<TableDto.ColumnDto> columnDtos;
    //目标
    private String tarDefinition;

    //转换失败备注
    private String conMsg;

    //迁移评估报告视图字段信息
    private String columns;

    //视图sql
    private String tarSql;

    //视图opgauss的options，例with(security_barrier=true)
    private String viewOptions;

}
