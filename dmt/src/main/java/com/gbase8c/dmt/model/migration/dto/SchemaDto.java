package com.gbase8c.dmt.model.migration.dto;

import com.google.common.collect.Lists;
import lombok.*;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
public class SchemaDto extends MigrationObject {

    private List<TableDto> tableDtos;
    private List<IndexDto> indexDtos;
    private List<ViewDto> viewDtos;
    private List<FunctionDto> functionDtos;
    private List<ConstraintDto> constraintDtos;
    private List<SequenceDto> sequenceDtos;
    private List<SynonymDto> synonymDtos;
    private List<TriggerDto> triggerDtos;

    private String tarSql;

    public String getName() {
        return name;
    }

    public String getTarName() {
        return tarName;
    }

    public List<TableDto> getTableDtos() {
        return tableDtos != null ? tableDtos : Lists.newArrayList();
    }

    public List<IndexDto> getIndexDtos() {
        return indexDtos != null ? indexDtos : Lists.newArrayList();
    }

    public List<ViewDto> getViewDtos() {
        return viewDtos != null ? viewDtos : Lists.newArrayList();
    }

    public List<FunctionDto> getFunctionDtos() {
        return functionDtos != null ? functionDtos : Lists.newArrayList();
    }

    public List<TriggerDto> getTriggerDtos() {
        return triggerDtos != null ? triggerDtos : Lists.newArrayList();
    }

    public List<ConstraintDto> getConstraintDtos() {
        return constraintDtos != null ? constraintDtos : Lists.newArrayList();
    }

    public List<SequenceDto> getSequenceDtos() {
        return sequenceDtos != null ? sequenceDtos : Lists.newArrayList();
    }

    public List<SynonymDto> getSynonymDtos() {
        return synonymDtos != null ? synonymDtos : Lists.newArrayList();
    }
}
