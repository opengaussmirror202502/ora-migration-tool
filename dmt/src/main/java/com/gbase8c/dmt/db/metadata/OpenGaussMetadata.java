package com.gbase8c.dmt.db.metadata;

import com.gbase8c.dmt.db.opengauss.*;
import com.gbase8c.dmt.db.opengauss.SequenceObjectImpl;
import com.gbase8c.dmt.db.opengauss.SynonymObjectImpl;
import com.gbase8c.dmt.model.migration.dto.DataSourceDto;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OpenGaussMetadata extends AbstractMetadata implements Metadata {

    public OpenGaussMetadata(DataSourceDto dto) {
        super(dto);
        this.meta = new MetaImpl(dto);
        this.synonymObject = new SynonymObjectImpl(dto);
        this.sequenceObject = new SequenceObjectImpl(dto);
        this.constraintObject = new ConstraintObjectImpl(dto);
        this.functionObject = new FunctionObjectImpl(dto);
        this.indexObject = new IndexObjectImpl(dto);
        this.roleObject = new RoleObjectImpl(dto);
        this.schemaObject = new SchemaObjectImpl(dto);
        this.tableObject = new TableObjectImpl(dto);
        this.columnObject = new ColumnObjectImpl(dto);
        this.tableSpaceObject = new TableSpaceObjectImpl(dto);
        this.userObject = new UserObjectImpl(dto);
        this.viewObject = new ViewObjectImpl(dto);
        this.triggerObject = new TriggerObjectImpl(dto);
    }

}
