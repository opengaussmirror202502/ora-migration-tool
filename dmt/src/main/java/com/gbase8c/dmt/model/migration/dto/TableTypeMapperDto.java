package com.gbase8c.dmt.model.migration.dto;

import lombok.*;

import java.math.BigDecimal;

@Data
public class TableTypeMapperDto {

    private String schema;

    private String tableName;

    private String primaryKey;

    private String foreignKey;

    private String firstColumn;

    //数据量大小
    private BigDecimal dataSize;

}
